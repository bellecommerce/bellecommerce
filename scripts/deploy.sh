#!/bin/bash
echo 'pull branch production'
WEBHOOK="https://discord.com/api/webhooks/983205751801675816/nXGjS2M6kIUxC9B0q0EcaJtxb_PazDdTE0bIutOcY-gf1AJ1c8N9UawLB6SSc6oXA5Ny"
##WEBHOOK="https://discord.com/api/webhooks/983826970833993788/g369Pfise5pNvkWO8TnCRbysXI1yAL4ceFRPUd3bKwjUpbyx2hFnQgjGvm74K1wwql_k"

cd /home/ubuntu/bellecommerce
git pull
sleep 2
scripts/clear-container.sh
cd manager-ui
git pull
# shellcheck disable=SC2103
cd ..
docker-compose build --build-arg APP_ENV="production"
docker-compose up -d

git log | head -n 7 > /tmp/git.log
if [ $? -eq 0 ]; then
  urlcommit="https://bitbucket.org/bellecommerce/bellecommerce/commits/"
  urlcommit="${urlcommit}$(cat /tmp/git.log | sed '/commit/q' | cut -d" " -f 2)"
  commit=$(cat /tmp/git.log | sed '/commit/q')
  message=$(cat /tmp/git.log | tail -n 2)

  MESSAGE=$( echo ${message} | sed 's/"/\"/g' | sed "s/'/\'/g" )
  URLCOMMIT=$( echo ${urlcommit} | sed 's/"/\"/g' | sed "s/'/\'/g" )
  COMMIT=$( echo ${commit} | sed 's/"/\"/g' | sed "s/'/\'/g" )

	./scripts/discord.sh \
    --webhook-url="$WEBHOOK" \
    --username "gvillela" \
    --avatar "https://i.imgur.com/12jyR5Q.png" \
    --text "Novo commit pull request aprovado" \
    --title "${MESSAGE}" \
    --description "${COMMIT}" \
    --color "0xFFFFFF" \
    --url "${URLCOMMIT}" \
    --author "Gustavo V. Goulart" \
    --author-icon "https://i.imgur.com/12jyR5Q.png" \
    --field "Author;Gustavo" \
    --field "Author;<gustavo@belltecnologia.com.br>" \
    --footer "Version 0.01" \
    --footer-icon "https://i.imgur.com/12jyR5Q.png" \
    --timestamp
fi
