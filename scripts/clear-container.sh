#!/bin/bash
echo "parando todos os containers"
for id in `docker ps -a -q`; do docker container stop $id; done
sleep 2
echo "Removendo todos os containers"
for id in `docker ps -a -q`; do docker container rm -f $id; done
echo "Removendo todas as imagens"
sleep 2
for id in `docker image ls -a -q`; do docker image rmi -f $id; done