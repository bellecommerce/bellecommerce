<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_profile', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->unsigned();
            $table->text('profile_photo_path');
            $table->string('cpf_cnpj', 25);
            $table->string('rg', 15);
            $table->date('birthday', 15);
            $table->string('gender', 20);
            $table->string('facebook', 255);
            $table->string('instagram', 255);
            $table->string('tiktok', 255);
            $table->string('whatsapp', 20);
            $table->string('mobile_phone', 20);
            $table->timestamps();
            $table->foreignId('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_profile');
    }
};
