<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stores', function (Blueprint $table) {
            $table->id();
            $table->string('store_code', 100);
            $table->integer('market_place_id');
            $table->integer('is_marketplace')->default(1);
            $table->integer('user_id')->unsigned();
            $table->string('name', 255)->nullable(false);
            $table->text('iframe_google_maps');
            $table->string('opening_hours', 100);
            $table->integer('status')->default(1);
            $table->text('domain');
            $table->text('subdomain');
            $table->timestamps();
            $table->foreignId('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stores');
    }
};
