<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Api\Notification\NotificationController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:api'], function () {
    Route::group(['prefix' => 'v1', 'namespace' => 'Api'], function () {
        Route::prefix('notification')->namespace('Notification')->group(function () {
            Route::get('/markAsReadNotification/{id}', [NotificationController::class, 'markAsReadNotification']);
            Route::get('/markAllReadNotifications', [NotificationController::class, 'markAllReadNotifications']);
            Route::get('/getAllNotification', [NotificationController::class, 'getAllNotification']);
            Route::delete('/deleteNotification', [NotificationController::class, 'deleteNotification']);
            Route::post('/sendNotificationToDatabase', [NotificationController::class, 'sendNotificationToDatabase']);
        });
    });
});
