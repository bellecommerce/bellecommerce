<?php

namespace App\Http\Controllers\Api\Notification;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Notifications\NewSaleMadeDatabase;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NotificationController extends Controller
{
    public function markAsReadNotification($id)
    {
        $result = ['status' => 200];
        try {
            $notification = Auth::user()->unreadNotifications()->where('id', $id)->get();
            $notification->map(function ($notification) {
                $result['data'] = $notification->markAsRead();
            });
        } catch (\Exception $exception) {
            $result = [
                'status' => 500,
                'error' => $exception->getMessage()
            ];
        }
        return response()->json($result, $result['status']);
    }

    public function markAllReadNotifications()
    {
        $result = ['status' => 200];
        $user = Auth::user();
        try {
            $notification = $user->unreadNotifications()->get();
            $notification->map(function ($notification) {
                $result['data'] = $notification->markAsRead();
            });
        } catch (\Exception $exception) {
            $result = [
                'status' => 500,
                'error' => $exception->getMessage()
            ];
        }
        return response()->json($result, $result['status']);
    }

    public function getAllNotification()
    {
        $result = ['status' => 200];
        try {
            $result['data'] = Auth::user()->notifications()->paginate(15);
        } catch (\Exception $exception) {
            $result = [
                'status' => 500,
                'error' => $exception->getMessage()
            ];
        }

        return response()->json($result, $result['status'],[], JSON_UNESCAPED_SLASHES);
    }

    public function sendNotificationToDatabase(Request $request)
    {
        $id = $request->input('id');
        $result = ['status' => 200];
        try {
            $user = User::where('manager_id', $id)->first();
            $result['data'] = $user->notify(new NewSaleMadeDatabase());
        } catch (\Exception $exception) {
            $result = [
                'status' => 500,
                'error' => $exception->getMessage()
            ];
        }
        return response()->json($result, $result['status']);
    }

    public function deleteNotification()
    {
        $result = ['status' => 200];
        $user = Auth::user();
        try {
            $notification = $user->notifications()->get();
            $notification->map(function ($notification) {
                $result['data'] = $notification->delete();
            });
        } catch (\Exception $exception) {
            $result = [
                'status' => 500,
                'error' => $exception->getMessage()
            ];
        }
        return response()->json($result, $result['status']);
    }
}
