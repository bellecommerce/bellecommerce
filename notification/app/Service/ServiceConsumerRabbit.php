<?php

namespace App\Service;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class ServiceConsumerRabbit
{
    private $channel;
    private AMQPStreamConnection $connection;
    public function __construct()
    {
        $this->connection = new AMQPStreamConnection(
            'rabbitmq-bell',
            5672,
            'guest',
            'guest'
        );
        $this->channel = $this->connection->channel();
    }
    public function getMessage($store)
    {
        $this->channel->exchange_declare(
            'store_notification_events',
            'direct',
            false,
            true,
            false,
            false,
            false
        );
        $this->channel->queue_declare('create_store_queue');
        $this->channel->queue_bind('create_store_queue', 'store_events', 'store_create');

        $storeMessage = new AMQPMessage($store);

        $this->channel->basic_publish($storeMessage, 'store_events', 'store_create');

        $this->channel->close();
        $this->connection->close();
    }
}
