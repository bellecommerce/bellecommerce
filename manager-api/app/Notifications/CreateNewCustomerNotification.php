<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Notifications;

use App\Models\Customer;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class CreateNewCustomerNotification extends Notification
{
    private Customer $customer;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Customer $customer)
    {
        $this->customer = $customer;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     */
    public function toMail($notifiable): MailMessage
    {
        return (new MailMessage)
            ->subject('Conta criada com sucesso')
            ->greeting('Olá ' . $this->customer->name)
            ->line('Seu cadastro foi criado com sucesso, clique no botão abaixo para acessar.')
            ->action('Acesso a Loja', url('https://belltecnologia.com.br'))
            ->line('Obrigado por usar nosso serviço');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
