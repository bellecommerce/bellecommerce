<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Http\Controllers\Api\WeddingList;

use App\Http\Controllers\Controller;
use App\Models\WeddingList;
use App\Services\WeddingListService;
use Illuminate\Http\Request;

class WeddingListController extends Controller
{
    protected WeddingListService $weddingListService;

    public function __construct(WeddingListService $weddingListService)
    {
        $this->weddingListService = $weddingListService;
    }
    public function index()
    {
        $lists = $this->weddingListService->listAll();
        return response()->json([
            'result' => $lists,
        ], 200);
    }


    public function create(Request $request)
    {
        $lists = $this->weddingListService->create($request);
        return response()->json([
            'result' => $lists,
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\WeddingList  $weddingList
     * @return \Illuminate\Http\Response
     */
    public function show(WeddingList $weddingList)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\WeddingList  $weddingList
     * @return \Illuminate\Http\Response
     */
    public function edit(WeddingList $weddingList)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\WeddingList  $weddingList
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WeddingList $weddingList)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\WeddingList  $weddingList
     * @return \Illuminate\Http\Response
     */
    public function destroy(WeddingList $weddingList)
    {
        //
    }
}
