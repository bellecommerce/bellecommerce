<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Http\Controllers\Api\Customer;

use App\Http\Controllers\Controller;
use App\Services\CustomerAddressService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CustomerAddressController extends Controller
{
    protected CustomerAddressService $customerAddressService;
    public function __construct(CustomerAddressService $customerAddressService)
    {
        $this->customerAddressService = $customerAddressService;
        $this->middleware('auth:api');
    }
    public function newAddress(Request $request): JsonResponse
    {
        $response = $this->customerAddressService->create($request);
        if (!$response) {
            return response()->json([
                'result' => false,
            ], 401);
        }
        return response()->json([
            'result' => $response,
        ], 200);
    }
    public function listAddress(int $customer_id): JsonResponse
    {
        $response = $this->customerAddressService->listAll($customer_id);
        return response()->json([
            'result' => $response,
        ], 200);
    }
    public function viewAddress(int $id): JsonResponse
    {
        $response = $this->customerAddressService->view($id);
        return response()->json([
            'result' => $response,
        ], 200);
    }
    public function updateAddress(Request $request): JsonResponse
    {
        $response = $this->customerAddressService->update($request);
        return response()->json([
            'result' => $response,
        ], 200);
    }
    public function deleteAddress($address_id): JsonResponse
    {
        $response = $this->customerAddressService->delete($address_id);
        return response()->json([
            'result' => $response,
        ], 200);
    }
}
