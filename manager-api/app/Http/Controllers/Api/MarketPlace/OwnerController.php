<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Http\Controllers\Api\MarketPlace;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Services\OwnService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class OwnerController extends Controller
{
    protected OwnService $ownService;

    public function __construct(OwnService $ownService)
    {
        $this->ownService = $ownService;
    }
    public function index()
    {
        //
    }

    public function create(Request $request): JsonResponse
    {
        $response = $this->ownService->create($request);

        if (!$response) {
            return response()->json([
                'result' => false,
            ], 401);
        }

        return response()->json([
            'result' => $response,
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $modelsOwnTenant
     * @return \Illuminate\Http\Response
     */
    public function show(User $modelsOwnTenant)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $modelsOwnTenant
     * @return \Illuminate\Http\Response
     */
    public function edit(User $modelsOwnTenant)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\User $modelsOwnTenant
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $modelsOwnTenant)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User $modelsOwnTenant
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $modelsOwnTenant)
    {
        //
    }
}
