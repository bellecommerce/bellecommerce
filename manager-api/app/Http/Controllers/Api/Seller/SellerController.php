<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Http\Controllers\Api\Seller;

use App\Http\Controllers\Controller;
use App\Models\Seller;
use App\Services\SellerService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class SellerController extends Controller
{
    protected SellerService $sellerService;

    public function __construct(SellerService $sellerService)
    {
        $this->sellerService = $sellerService;
    }
    public function index(): JsonResponse
    {
        $sellers = $this->sellerService->listAll();
        return response()->json([
            'seller' => $sellers,
        ], 200);
    }

    public function create(Request $request): JsonResponse
    {
        $response = $this->sellerService->create($request);

        if (!$response) {
            return response()->json([
                'result' => false,
            ], 401);
        }

        return response()->json([
            'result' => $response,
        ], 200);
    }

    public function show(Seller $seller)
    {
        //
    }


    public function edit(Seller $seller)
    {
        //
    }


    public function update(Request $request)
    {
        $response = $this->sellerService->update($request);

        if (!$response) {
            return response()->json([
                'result' => false,
            ], 401);
        }

        return response()->json([
            'result' => true,
        ], 200);
    }

    public function destroy(Seller $seller)
    {
        //
    }
}
