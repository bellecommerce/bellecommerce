<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Http\Controllers\Api\Users;

use App\Http\Controllers\Controller;
use App\Services\UserService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class UserController extends Controller
{
    protected UserService $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }
    public function profile(): JsonResponse
    {
        $result = ['status' => 200];
        $user = \Auth::id();
        try {
            $result['data'] = $this->userService->getProfile($user);
        } catch (\Exception $exception) {
            $result = [
                'status' => 500,
                'error' => $exception->getMessage()
            ];
        }
        return response()->json($result, $result['status'], [], JSON_UNESCAPED_SLASHES);
    }

    public function create(Request $request): JsonResponse
    {
        $response = $this->userService->create($request);

        if (!$response) {
            return response()->json([
                'result' => false,
            ], 401);
        }

        return response()->json([
            'result' => $response,
        ], 200);
    }

    public function view(): JsonResponse
    {
        $user_id = \Auth::id();
        $result = ['status' => 200];
        try {
            $result['data'] = $this->userService->view($user_id);
        } catch (\Exception $exception) {
            $result = [
                'status' => 500,
                'error' => $exception->getMessage()
            ];
        }
        return response()->json($result, $result['status'], [], JSON_UNESCAPED_SLASHES);
    }

    public function update(Request $request): JsonResponse
    {
        $result = ['status' => 200];
        try {
            $result['data'] = $this->userService->update($request);
        } catch (\Exception $exception) {
            $result = [
                'status' => 500,
                'error' => $exception->getMessage()
            ];
        }
        return response()->json($result, $result['status'], [], JSON_UNESCAPED_SLASHES);
    }

    public function changePhoto(Request $request): JsonResponse
    {
        $result = ['status' => 200];
        try {
            $result['data'] = $this->userService->changePhoto($request);
        } catch (\Exception $exception) {
            $result = [
                'status' => 500,
                'error' => $exception->getMessage()
            ];
        }
        return response()->json($result, $result['status'], [], JSON_UNESCAPED_SLASHES);
    }
    public function changePassword(Request $request): JsonResponse
    {
        $result = ['status' => 200];
        try {
            $result['data'] = $this->userService->changePassword($request);
        } catch (\Exception $exception) {
            $result = [
                'status' => 500,
                'error' => $exception->getMessage()
            ];
        }
        return response()->json($result, $result['status']);
    }
}
