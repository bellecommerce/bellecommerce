<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Http\Controllers\Api\BrandModel;

use App\Http\Controllers\Controller;
use App\Services\BrandModelService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class BrandModelController extends Controller
{
    protected BrandModelService $brandModelService;

    public function __construct(BrandModelService $brandModelService)
    {
        $this->brandModelService = $brandModelService;
    }
    public function create(Request $request): JsonResponse
    {
        $response = $this->brandModelService->create($request);
        if (!$response) {
            return response()->json([
                'result' => false,
            ], 401);
        }
        return response()->json([
            'result' => $response,
        ], 200);
    }
    public function list($id): JsonResponse
    {
        $response = $this->brandModelService->list($id);
        if (!$response)
            $response = 'Nenhuma marca cadastrada';
        return response()->json([
            'result' => $response,
        ], 200);
    }

}
