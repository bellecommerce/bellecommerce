<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Http\Controllers\Api\Store;

use App\Http\Controllers\Controller;
use App\Services\StoreService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class StoreController extends Controller
{
    protected StoreService $storeService;

    public function __construct(StoreService $storeService)
    {
        $this->storeService = $storeService;
    }
    public function index(): JsonResponse
    {
        $result = ['status' => Response::HTTP_OK];
        try {
            $result['data'] = $this->storeService->listAll();
        } catch (\Exception $exception) {
            $result = [
                'status' => Response::HTTP_INTERNAL_SERVER_ERROR,
                'error' => $exception->getMessage()
            ];
        }
        return response()->json($result, $result['status'], [], JSON_UNESCAPED_SLASHES);
    }

    public function getStore(): JsonResponse
    {
        $user_id = \Auth::id();
        $result = ['status' => Response::HTTP_OK];
        try {
            $result['data'] = $this->storeService->getStore($user_id);
        } catch (\Exception $exception) {
            $result = [
                'status' => Response::HTTP_INTERNAL_SERVER_ERROR,
                'error' => $exception->getMessage()
            ];
        }
        return response()->json($result, $result['status'], [], JSON_UNESCAPED_SLASHES);
    }

    public function create(Request $request): JsonResponse
    {
        $result = ['status' => Response::HTTP_CREATED];
        try {
            $result['data'] = $this->storeService->create($request);
        } catch (\Exception $exception) {
            $result = [
                'status' => Response::HTTP_INTERNAL_SERVER_ERROR,
                'error' => $exception->getMessage()
            ];
        }
        return response()->json($result, $result['status'], [], JSON_UNESCAPED_SLASHES);
    }

    public function view($id): JsonResponse
    {
        $result = ['status' => Response::HTTP_OK];
        try {
            $result['data'] = $this->storeService->view($id);
        } catch (\Exception $exception) {
            $result = [
                'status' => Response::HTTP_INTERNAL_SERVER_ERROR,
                'error' => $exception->getMessage()
            ];
        }
        return response()->json($result, $result['status'], [], JSON_UNESCAPED_SLASHES);
    }

    public function update(Request $request): JsonResponse
    {
        $response = $this->storeService->update($request);

        if (!$response) {
            return response()->json([
                'result' => false,
            ], 401);
        }

        return response()->json([
            'result' => true,
        ], 200);
    }

    public function delete($id): JsonResponse
    {
        $response = $this->storeService->delete($id);

        if (!$response) {
            return response()->json([
                'result' => false,
            ], 401);
        }

        return response()->json([
            'result' => true,
        ], 200);
    }
}
