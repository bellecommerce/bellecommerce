<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Http\Controllers\Api\Media;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreMediaImage;
use App\Services\MediaService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class MediaController extends Controller
{
    protected MediaService $mediaService;
    public function __construct(MediaService $mediaService)
    {
        $this->mediaService = $mediaService;
    }

    /**
     *
     * @param StoreMediaImage $request
     * @return JsonResponse
     */
    public function create(StoreMediaImage $request): JsonResponse
    {
        $result = ['status' => Response::HTTP_CREATED];
        try {
            $result['data'] = $this->mediaService->create($request);
        } catch (\Exception $exception) {
            $result = [
                'status' => Response::HTTP_INTERNAL_SERVER_ERROR,
                'error' => $exception->getMessage()
            ];
        }
        return response()->json($result, $result['status'], [], JSON_UNESCAPED_SLASHES);
    }
    public function index($id)
    {
        $result = ['status' => Response::HTTP_OK];
        try {
            $result['data'] = $this->mediaService->listAll($id);
        } catch (\Exception $exception) {
            $result = [
                'status' => Response::HTTP_INTERNAL_SERVER_ERROR,
                'error' => $exception->getMessage()
            ];
        }
        return response()->json($result, $result['status'], [], JSON_UNESCAPED_SLASHES);
    }
    public function delete($id)
    {
        $result = ['status' => Response::HTTP_NO_CONTENT];
        try {
            $result['data'] = $this->mediaService->delete($id);
        } catch (\Exception $exception) {
            $result = [
               'status' => Response::HTTP_INTERNAL_SERVER_ERROR,
               'error' => $exception->getMessage()
            ];
        }
        return response()->json($result, $result['status']);
    }

    public function imageDescription(Request $request)
    {
        $result = ['status' => Response::HTTP_OK];
        try {
            $result['data'] = $this->mediaService->imageDescription($request);
        } catch (\Exception $exception) {
            $result = [
                'status' => Response::HTTP_INTERNAL_SERVER_ERROR,
                'error' => $exception->getMessage()
            ];
        }
        return response()->json($result, $result['status'], [], JSON_UNESCAPED_SLASHES);
    }
}
