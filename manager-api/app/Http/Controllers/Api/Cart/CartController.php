<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Http\Controllers\Api\Cart;

use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{

    public function getUser()
    {
        return Auth::user()->id;
    }

    public function index()
    {
        $user = $this->getUser();
        $cart = Cart::with('products', 'customer')
            ->where('customer_id', '=', $user)
            ->get();
        return response()->json([
            'result' => $cart,
        ], 200);
    }

    public function create($id)
    {
        $user = $this->getUser();
        $product = Product::find($id);
        $cart = Cart::create([
            'customer_id' => $user,
            'product_id' => $product->id
        ]);
        return response()->json([
            'result' => $cart,
        ], 200);
    }

    public function destroy($id)
    {
        $item = Cart::with('products', 'customer')
            ->where('product_id', '=', $id)
            ->first();
        $result = $item->delete();
        if ($result) {
            return $this->index();
        }
    }
}
