<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Http\Controllers\Api\Contracts;

use App\Http\Controllers\Controller;
use App\Models\Contract;
use App\Models\Store;
use Illuminate\Http\Request;

class ContractController extends Controller
{

    public function index()
    {
        $contract = Contract::all();
        if (!$contract) {
            return response()->json([
                'result' => false,
            ], 401);
        }
        return response()->json([
            'result' => $contract,
        ], 200);
    }

    public function create(Request $request)
    {
        $data = $request->all();
        $contract = Contract::create($data);
        foreach ($data['conditions'] as $condition) {
            $contract->conditions()->create($condition);
        }

        return response()->json([
           'result' => $contract,
        ], 200);
    }

    public function show(int $idContract)
    {
        $contract = Contract::with('conditions')->where('id', $idContract)->get();
        return response()->json([
                'result' => $contract,
            ], 200);
    }

    public function attach(Request $request)
    {
        $store_id = $request->input('store_id');
        $contract_id = $request->input('contract_id');

        $store = Store::find($store_id);

        $result = $store->contract()->attach($contract_id);
        return response()->json([
            'result' => $result,
        ], 200);
    }

    public function update(Request $request)
    {
        $data = $request->all();
        $contract = Contract::where('id', $request->input('id'))
            ->update(
                [
                    'name' => $request->input('name'),
                    'description' => $request->input('description'),
                    'tag' => $request->input('tag')
                ]
            );
        $contractUpdate = Contract::with('conditions')
            ->where('id', $request->input('id'))
            ->get();

        foreach ($data['conditions'] as $condition) {
            foreach ($contractUpdate as $conditions) {
                $conditions->conditions->find($condition['id'])->update($condition);
            }
        }

        return response()->json([
            'result' => $contract,
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Contract  $modelsContract
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contract $modelsContract)
    {
        //
    }
}
