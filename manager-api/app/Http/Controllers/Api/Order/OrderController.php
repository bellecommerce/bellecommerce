<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Http\Controllers\Api\Order;

use App\Http\Controllers\Controller;
use App\Services\OrderService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    protected OrderService $orderService;
    public function __construct(OrderService $orderService)
    {
        $this->orderService = $orderService;
    }

    public function create(Request $request): JsonResponse
    {
        $response = $this->orderService->create($request);
        if (!$response) {
            return response()->json([
                'result' => false,
            ], 401);
        }
        return response()->json([
            'result' => $response,
        ], 200);
    }
    public function list(): JsonResponse
    {
        $result = $this->orderService->list();
        if (!$result->count()) {
            return response()->json([
                'result' => "Nenhuma ordem encontrada!",
            ], 401);
        }
        return response()->json([
            'result' => $result,
        ], 200);
    }
}
