<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Http\Controllers\Api\Product;

use App\Http\Controllers\Controller;
use App\Models\Color;
use App\Models\Store;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ColorController extends Controller
{

    public function index($store_id): JsonResponse
    {
        $result = ['status' => 200];
        try {
            $result['data'] = Color::where('store_id', $store_id)->get();
        } catch (\Exception $exception) {
            $result = [
                'status' => 500,
                'error' => $exception->getMessage()
            ];
        }
        return response()->json($result, $result['status']);
    }


    public function store(Request $request): JsonResponse
    {
        $result = ['status' => 200];
        $colors = $request->all();
        try {
            $store = Store::find($colors['store_id']);
            $result['data'] = $store->color()->create($colors);
        } catch (\Exception $exception) {
            $result = [
                'status' => 500,
                'error' => $exception->getMessage()
            ];
        }
        return response()->json($result, $result['status']);
    }


    public function show($id): JsonResponse
    {
        $result = ['status' => 200];
        try {
            $result['data'] = Color::find($id);
        } catch (\Exception $exception) {
            $result = [
                'status' => 500,
                'error' => $exception->getMessage()
            ];
        }
        return response()->json($result, $result['status']);
    }


    public function update(Request $request, $id): JsonResponse
    {
        $newColor = $request->all();
        $color = Color::find($id);
        $result = $color->update($newColor);
        return response()->json([
            'result' => $result,
        ], 200);
    }


    public function destroy($id): JsonResponse
    {
        $color = Color::find($id);
        $result = $color->delete();
        return response()->json([
            'result' => $result,
        ], 200);
    }
}
