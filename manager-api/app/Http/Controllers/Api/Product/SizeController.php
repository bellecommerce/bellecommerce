<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Http\Controllers\Api\Product;

use App\Http\Controllers\Controller;
use App\Models\Size;
use App\Models\Store;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class SizeController extends Controller
{

    public function index(int $store_id): JsonResponse
    {
        $result = ['status' => 200];
        try {
            $result['data'] = Size::where('store_id', $store_id)->get();
        } catch (\Exception $exception) {
            $result = [
                'status' => 500,
                'error' => $exception->getMessage()
            ];
        }
        return response()->json($result, $result['status']);
    }


    public function store(Request $request): JsonResponse
    {
        $result = ['status' => 200];
        $size = $request->all();
        try {
            $store = Store::find($size['store_id']);
            $result['data'] = $store->size()->create($size);
        } catch (\Exception $exception) {
            $result = [
                'status' => 500,
                'error' => $exception->getMessage()
            ];
        }
        return response()->json($result, $result['status']);
    }


    public function show($id): JsonResponse
    {
        $result = ['status' => 200];
        try {
            $result['data'] = Size::find($id);
        } catch (\Exception $exception) {
            $result = [
                'status' => 500,
                'error' => $exception->getMessage()
            ];
        }
        return response()->json($result, $result['status']);
    }


    public function update(Request $request, $id): JsonResponse
    {
        $newSize = $request->all();
        $size = Size::find($id);
        $result = $size->update($newSize);
        return response()->json([
            'result' => $result,
        ], 200);
    }


    public function destroy($id): JsonResponse
    {
        $size = Size::find($id);
        $result = $size->delete();
        return response()->json([
            'result' => $result,
        ], 200);
    }
}
