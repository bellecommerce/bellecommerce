<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Http\Controllers\Api\Product;

use App\Http\Controllers\Controller;
use App\Services\ProductService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ProductController extends Controller
{
    protected ProductService $productService;
    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }
    public function index($store_id, $paginate): JsonResponse
    {
        $result = ['status' => Response::HTTP_OK];
        try {
            $result['data'] = $this->productService->listAll($store_id, $paginate);
        } catch (\Exception $exception) {
            $result = [
                'status' => 500,
                'error' => $exception->getMessage()
            ];
        }
        return response()->json($result, $result['status'], [], JSON_UNESCAPED_SLASHES);
    }

    public function create(Request $request): JsonResponse
    {
        $result = ['status' => Response::HTTP_CREATED];
        try {
            $result['data'] = $this->productService->create($request);
        } catch (\Exception $exception) {
            $result = [
                'status' => 500,
                'error' => $exception->getMessage()
            ];
        }
        return response()->json($result, $result['status']);
    }

    public function activeInactiveProduct($id): JsonResponse
    {
        $result = ['status' => Response::HTTP_NO_CONTENT];
        try {
            $this->productService->activeInactiveProduct($id);
        } catch (\Exception $exception) {
            $result = [
                'status' => Response::HTTP_INTERNAL_SERVER_ERROR,
                'error' => $exception->getMessage()
            ];
        }
        return response()->json($result, $result['status']);
    }
    public function view($product_id): JsonResponse
    {
        $result = ['status' => Response::HTTP_OK];
        if ($product_id === null) {
            $result = [
                'status' => Response::HTTP_NOT_FOUND,
                'error' => 'Parametro de consulta inválido.'
            ];
        }
        try {
            $result['data'] = $this->productService->view($product_id);
        } catch (\Exception $exception) {
            $result = [
                'status' => Response::HTTP_INTERNAL_SERVER_ERROR,
                'error' => $exception->getMessage()
            ];
        }
        return response()->json($result, $result['status']);
    }
    public function update(Request $request): JsonResponse
    {
        $result = ['status' => Response::HTTP_ACCEPTED];
        try {
            $result['data'] = $this->productService->update($request);
        } catch (\Exception $exception) {
            $result = [
              'status' => Response::HTTP_INTERNAL_SERVER_ERROR,
              'error' => $exception->getMessage()
            ];
        }
        return response()->json($result, $result['status']);
    }

    public function delete($product_id): JsonResponse
    {
        $response = $this->productService->delete($product_id);
        return response()->json([
            'result' => $response,
        ], 200);
    }
}
