<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Http\Controllers\Api\Product;

use App\Http\Controllers\Controller;
use App\Models\OtherFeature;
use App\Models\Store;
use App\Models\TechnicalData;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use PHPUnit\Exception;

class OtherFeatureController extends Controller
{

    public function index(int $store_id): JsonResponse
    {
        $result = ['status' => 200];
        try {
            $result['data'] = OtherFeature::with('other_feature_item')
                ->where('store_id', '=', $store_id)->get();
        } catch (\Exception $exception) {
            $result = [
                'status' => 500,
                'error' => $exception->getMessage()
            ];
        }
        return response()->json($result, $result['status']);
    }


    public function store(Request $request): JsonResponse
    {
        $result = ['status' => 200];
        $data = $request->all();
        $store = Store::find($data['store_id']);
        try {
            $otherFeature = $store->other_feature()->create(["name" => $data['name']]);
            foreach ($data['itens'] as $item) {
                $otherFeature->other_feature_item()->create($item);
            }
            $result['data'] = $otherFeature;
        } catch (\Exception $exception) {
            $result = [
                'status' => 500,
                'error' => $exception->getMessage()
            ];
        }
        return response()->json($result, $result['status']);
    }


    public function show($id): JsonResponse
    {
       //
    }


    public function update(Request $request, $id): JsonResponse
    {
        //
    }


    public function destroy($id): JsonResponse
    {
        //
    }
}
