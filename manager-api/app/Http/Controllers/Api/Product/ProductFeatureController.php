<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Http\Controllers\Api\Product;

use App\Http\Controllers\Controller;
use App\Services\ProductFeatureService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ProductFeatureController extends Controller
{
    protected ProductFeatureService $productFeatureService;
    public function __construct(ProductFeatureService $productFeatureService)
    {
        $this->productFeatureService = $productFeatureService;
    }
    public function store(Request $request): JsonResponse
    {
        $result = ['status' => 200];
        try {
            $result['data'] = $this->productFeatureService->create($request);
        } catch (\Exception $exception) {
            $result = [
                'status' => 500,
                'error' => $exception->getMessage()
            ];
        }
        return response()->json($result, $result['status']);
    }
    public function list()
    {
        //
    }
    public function view($product_id): JsonResponse
    {
        $result = ['status' => 200];
        try {
            $result['data'] = $this->productFeatureService->view($product_id);
            if ($product_id === null) {
                $result['data'] = 'Parametro de consulta inválido.';
            }
            if (empty($result['data'])) {
                $result['data'] = 'Nenhum produto encontrado';
            }
        } catch (\Exception $exception) {
            $result = [
                'status' => 500,
                "error" => $exception->getMessage()
            ];
        }
        return response()->json($result, $result['status']);
    }
    public function update(Request $request): JsonResponse
    {
        $result = ['status' => 200];
        try {
            $result['data'] = $this->productFeatureService->updateFeatures($request);
        } catch (\Exception $exception) {
            $result = [
                'status' => 500,
                "error" => $exception->getMessage()
            ];
        }
        return response()->json($result, $result['status']);
    }
    public function delete($product_id): JsonResponse
    {
        $response = $this->productFeatureService->delete($product_id);
        return response()->json([
            'result' => $response,
        ], 200);
    }
}
