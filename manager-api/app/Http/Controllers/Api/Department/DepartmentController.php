<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Http\Controllers\Api\Department;

use App\Http\Controllers\Controller;
use App\Services\DepartmentService;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Symfony\Component\HttpFoundation\Response;

class DepartmentController extends Controller
{
    protected DepartmentService $departmentService;

    public function __construct(DepartmentService $departmentService)
    {
        $this->departmentService = $departmentService;
    }

    public function index($store_id, $paginate)
    {
        $result = ['status' => Response::HTTP_OK];
        try {
            $result['data'] = $this->departmentService->listAllDepartmentAndCategory($store_id, $paginate);
        } catch (\Exception $exception) {
            $result = [
                'status' => Response::HTTP_INTERNAL_SERVER_ERROR,
                'error' => $exception->getMessage()
            ];
        }
        return response()->json($result, $result['status'], [], JSON_UNESCAPED_SLASHES);
    }

    public function create(Request $request)
    {
        $result = ['status' => Response::HTTP_CREATED];
        try {
            $result['data'] = $this->departmentService->create($request);
        } catch (\Exception $exception) {
            $result = [
                'status' => Response::HTTP_INTERNAL_SERVER_ERROR,
                'error' => $exception->getMessage()
            ];
        }
        return response()->json($result, $result['status']);
    }

    public function activeInactive($id)
    {
        $result = ['status' => Response::HTTP_NO_CONTENT];
        try {
            $this->departmentService->activeInactive($id);
        } catch (\Exception $exception) {
            $result = [
                'status' => Response::HTTP_INTERNAL_SERVER_ERROR,
                'error' => $exception->getMessage()
            ];
        }
        return response()->json($result, $result['status']);
    }

    public function view($id)
    {
        $result = ['status' => Response::HTTP_OK];
        try {
            $result['data'] = $this->departmentService->view($id);
        } catch (\Exception $exception) {
            $result = [
                'status' => Response::HTTP_INTERNAL_SERVER_ERROR,
                'error' => $exception->getMessage()
            ];
        }
        return response()->json($result, $result['status']);
    }

    public function update(Request $request)
    {
        $result = ['status' => Response::HTTP_ACCEPTED];
        try {
            $result['data'] = $this->departmentService->update($request);
        } catch (\Exception $exception) {
            $result = [
                'status' => Response::HTTP_INTERNAL_SERVER_ERROR,
                'error' => $exception->getMessage()
            ];
        }
        return response()->json($result, $result['status']);
    }
}
