<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Http\Controllers\Api\Coupon;

use App\Http\Controllers\Controller;
use App\Services\CouponService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CouponController extends Controller
{
    protected CouponService $couponService;

    public function __construct(CouponService $couponService)
    {
        $this->couponService = $couponService;
    }
    public function create(Request $request): JsonResponse
    {
        $result = ['status' => Response::HTTP_CREATED];
        try {
            $result['data'] = $this->couponService->create($request);
        } catch (\Exception $exception) {
            $result = [
                'status' => 500,
                'error' => $exception->getMessage()
            ];
        }
        return response()->json($result, $result['status']);
    }
    public function index($id, $paginate): JsonResponse
    {
        $result = ['status' => Response::HTTP_OK];
        try {
            $result['data'] = $this->couponService->listAll($id, $paginate);
        } catch (\Exception $exception) {
            $result = [
                'status' => Response::HTTP_INTERNAL_SERVER_ERROR,
                'error' => $exception->getMessage()
            ];
        }
        return response()->json($result, $result['status'], [], JSON_UNESCAPED_SLASHES);
    }
    public function search($code): JsonResponse
    {
        $result = ['status' => Response::HTTP_FOUND];
        try {
            $result['data'] = $this->couponService->search($code);
        } catch (\Exception $exception) {
            $result = [
                'status' => Response::HTTP_INTERNAL_SERVER_ERROR,
                'error' => $exception->getMessage()
            ];
        }
        return response()->json($result, $result['status']);
    }

    public function view($id): JsonResponse
    {
        $result = ['status' => Response::HTTP_OK];
        try {
            $result['data'] = $this->couponService->view($id);
        } catch (\Exception $exception) {
            $result = [
                'status' => Response::HTTP_INTERNAL_SERVER_ERROR,
                'error' => $exception->getMessage()
            ];
        }
        return response()->json($result, $result['status']);
    }

    public function update(Request $request): JsonResponse
    {
        $result = ['status' => Response::HTTP_ACCEPTED];
        try {
            $result['data'] = $this->couponService->update($request);
        } catch (\Exception $exception) {
            $result = [
                'status' => Response::HTTP_INTERNAL_SERVER_ERROR,
                'error' => $exception->getMessage()
            ];
        }
        return response()->json($result, $result['status']);
    }
}
