<?php

namespace App\Listeners;

use App\Events\ActiveCategory;
use Exception;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class ActiveCategoryListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param \App\Events\ActiveCategory $event
     * @return void
     * @throws Exception
     */
    public function handle(ActiveCategory $event)
    {
        $event->active();
    }
}
