<?php

namespace App\Listeners;

use App\Events\ActiveProduct;
use Exception;

class ActiveProductListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param \App\Events\ActiveProduct $event
     * @throws Exception
     */
    public function handle(ActiveProduct $event)
    {
        $event->active();
    }
}
