<?php

namespace App\Listeners;

use App\Events\ActiveDepartment;
use Exception;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class ActiveDepartmentListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param \App\Events\ActiveDepartment $event
     * @return void
     * @throws Exception
     */
    public function handle(ActiveDepartment $event)
    {
        $event->active();
    }
}
