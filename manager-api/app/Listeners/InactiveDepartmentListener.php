<?php

namespace App\Listeners;

use App\Events\InactiveDepartment;
use Exception;

class InactiveDepartmentListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param \App\Events\InactiveDepartment $event
     * @return void
     * @throws Exception
     */
    public function handle(InactiveDepartment $event)
    {
        $event->inactive();
    }
}
