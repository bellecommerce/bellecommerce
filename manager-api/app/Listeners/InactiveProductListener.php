<?php

namespace App\Listeners;

use App\Events\InactiveProduct;
use Exception;

class InactiveProductListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param \App\Events\InactiveProduct $event
     * @return void
     * @throws Exception
     */
    public function handle(InactiveProduct $event)
    {
        $event->inactive();
    }
}
