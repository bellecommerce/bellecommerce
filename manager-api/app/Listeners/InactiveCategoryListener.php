<?php

namespace App\Listeners;

use App\Events\InactiveCategory;
use Exception;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class InactiveCategoryListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param \App\Events\InactiveCategory $event
     * @return void
     * @throws Exception
     */
    public function handle(InactiveCategory $event)
    {
        $event->inactive();
    }
}
