<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Features extends Model
{
    protected $fillable = [
        'model_id',
        'product_id',
        'status',
        'size_id',
        'color_id',
        'technical_data_id',
        'other_feature_id',
        'sku',
        'weight',
        'height',
        'width',
        'length',
        'description',
        'feature_stock',
        'is_feature_plus_sale',
        'feature_bar_code',
        'feature_price',
        'feature_discount_price'
    ];
    use HasFactory;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'features';
    protected $primaryKey = 'id';
    protected $casts = [
        'feature_price' => 'float',
        'feature_discount_price' => 'float',
        'model_id' => 'int',
        'product_id' => 'int',
        'status' => 'int',
        'size_id' => 'int',
        'color_id' => 'int',
        'technical_data_id' => 'int',
        'other_feature_id' => 'int'
    ];

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class, 'product_id');
    }
    public function media(): HasMany
    {
        return $this->hasMany(MediaProducts::class, 'feature_product_id');
    }
    public function brand_model(): BelongsTo
    {
        return $this->belongsTo(BrandModel::class, 'model_id');
    }
    public function color(): BelongsTo
    {
        return $this->belongsTo(Color::class, 'color_id');
    }
    public function size(): BelongsTo
    {
        return $this->belongsTo(Size::class, 'size_id');
    }
    public function technical_data(): BelongsTo
    {
        return $this->belongsTo(TechnicalData::class, 'technical_data_id')
            ->with('technical_data_item');
    }
    public function other_feature(): BelongsTo
    {
        return $this->belongsTo(OtherFeature::class, 'other_feature_id')
            ->with('other_feature_item');
    }
}
