<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Product extends Model
{
    protected $fillable = [
        'store_id',
        'department_id',
        'ref',
        'name',
        'description',
        'is_start',
        'status',
        'meta_google_title',
        'meta_google_description',
        'meta_google_keywords',
        'send_google_merchant',
        'warranty',
        'brand_id',
        'model_id',
        'is_plus_sale',
    ];
    use HasFactory;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'products';
    protected $primaryKey = 'id';
    protected $casts = [
        'price' => 'float',
        'discount_price' => 'float'
    ];

    protected $hidden = ['pivot'];

    public function store(): BelongsTo
    {
        return $this->belongsTo(Store::class, 'store_id');
    }
    public function categories(): BelongsToMany
    {
        return $this->belongsToMany(
            Category::class,
            'product_category',
            'product_id',
            'category_id'
        );
    }
    public function subcategories(): BelongsToMany
    {
        return $this->belongsToMany(
            SubCategory::class,
            'product_subcategory',
            'product_id',
            'subcategory_id'
        );
    }
    public function department(): BelongsTo
    {
        return $this->belongsTo(Department::class, 'department_id');
    }
    public function brand(): BelongsTo
    {
        return $this->belongsTo(Brand::class, 'brand_id');
    }
    public function feature(): HasMany
    {
        return $this->hasMany(Features::class, 'product_id')
            ->with('color', 'size', 'technical_data', 'other_feature', 'media');
    }
    public function orders(): BelongsToMany
    {
        return $this->belongsToMany(
            Order::class,
            'order_products',
            'product_id',
            'order_id'
        )->withPivot(['amount']);
    }
    public function weddingList(): BelongsToMany
    {
        return $this->belongsToMany(
            WeddingList::class,
            'wedding_list_items',
            'product_id',
            'wedding_list_id'
        );
    }
    public function wishlist(): BelongsToMany
    {
        return $this->belongsToMany(
            Wishlist::class,
            'wishlist_items',
            'product_id',
            'wishlist_id'
        );
    }
    public function rating(): HasMany
    {
        return $this->hasMany(Rating::class, 'product_id');
    }
    public function cart(): HasOne
    {
        return $this->hasOne(Cart::class, 'product_id');
    }

    public function coupons(): BelongsToMany
    {
        return $this->belongsToMany(
            Coupon::class,
            'coupon_products',
            'product_id',
            'coupon_id'
        );
    }
}
