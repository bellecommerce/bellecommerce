<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Contract extends Model
{
    use HasFactory;

    protected $table = 'contract';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name',
        'description',
        'tag'
    ];
    protected $hidden = [
        "created_at",
        "updated_at"
    ];

    public function conditions(): HasMany
    {
        return $this->hasMany(ContractCondition::class, 'contract_id');
    }
    public function stores(): BelongsToMany
    {
        return $this->belongsToMany(
            Store::class,
            'store_contract',
            'contract_id',
            'store_id'
        );
    }
}
