<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Coupon extends Model
{
    protected $fillable = [
        'code',
        'percentage',
        'amount',
        'expiration',
        'ship',
        'product',
        'category',
        'description',
        'status',
        'quantity'
    ];
    use HasFactory;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'coupons';
    protected $primaryKey = 'id';
    protected $casts = [
        'amount' => 'float',
        'percentage' => 'int',
        'quantity' => 'int'
    ];

    public function order(): HasMany
    {
        return $this->hasMany(Order::class, 'order_id');
    }
    public function store(): BelongsTo
    {
        return $this->belongsTo(Store::class, 'store_id');
    }
    public function categories(): BelongsToMany
    {
        return $this->belongsToMany(
            Category::class,
            'coupon_category',
            'coupon_id',
            'category_id'
        );
    }

    public function products(): BelongsToMany
    {
        return $this->belongsToMany(
            Product::class,
            'coupon_products',
            'coupon_id',
            'product_id'
        );
    }
}
