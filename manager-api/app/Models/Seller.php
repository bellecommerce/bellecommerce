<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Seller extends User
{
    use HasFactory;

    protected $table = 'users';

    /**
     * Create a new Eloquent model instance.
     *
     * @param  array  $attributes
     * @return void
     */

    public function __construct(array $attributes = [])
    {
        $this->fillable = array_merge($this->fillable, ['cnpj']);
        parent::__construct($attributes);
    }

    public function sellerStore(): HasMany
    {
        return $this->hasMany(SellerStore::class, 'seller_id');
    }
}
