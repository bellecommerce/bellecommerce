<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Models;

use App\MarketPlace\Traits\TenantTraitStore;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Store extends Model
{
    use HasFactory, TenantTraitStore;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'stores';
    protected $primaryKey = 'id';

    protected $fillable = [
        'store_code',
        'is_marketplace',
        'name',
        'phone',
        'whatsapp',
        'cnpj',
        'address',
        'iframe_google_maps',
        'opening_hours',
        'status',
        'domain',
        'subdomain',
        'facebook',
        'instagram',
        'youtube',
        'tiktok'
    ];

    protected $hidden = [
        'user_id',
        'market_place_id'
    ];

    public function profile(): HasOne
    {
        return $this->hasOne(StoreProfile::class, 'store_id');
    }
    public function marketplace(): BelongsTo
    {
        return $this->belongsTo(MarketPlace::class, 'market_place_id');
    }
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    public function category(): HasMany
    {
        return $this->hasMany(Category::class, 'store_id');
    }
    public function department(): HasMany
    {
        return $this->hasMany(Department::class, 'store_id');
    }
    public function customers(): BelongsToMany
    {
        return $this->belongsToMany(
            Customer::class,
            'customer_stores',
            'store_id',
            'customer_id'
        );
    }
    public function product(): HasMany
    {
        return $this->hasMany(Product::class, 'store_id')
            ->with('feature');
    }
    public function brand(): HasMany
    {
        return $this->hasMany(Brand::class, 'store_id')
            ->with('brand_model');
    }
    public function order(): HasMany
    {
        return $this->hasMany(Order::class, 'store_id');
    }
    public function coupon(): HasMany
    {
        return $this->hasMany(Coupon::class, 'store_id');
    }
    public function payments(): HasMany
    {
        return $this->hasMany(Payment::class, 'store_id');
    }
    public function color(): HasMany
    {
        return $this->hasMany(Color::class, 'store_id');
    }
    public function size(): HasMany
    {
        return $this->hasMany(Size::class, 'store_id');
    }
    public function technical_data(): HasMany
    {
        return $this->hasMany(TechnicalData::class, 'store_id');
    }
    public function other_feature(): HasMany
    {
        return $this->hasMany(OtherFeature::class, 'store_id');
    }
    public function contract(): BelongsToMany
    {
        return $this->belongsToMany(
            Contract::class,
            'store_contract',
            'store_id',
            'contract_id'
        );
    }
    public function banners(): HasMany
    {
        return $this->hasMany(Banner::class, 'store_id');
    }
    public function weddingList(): HasMany
    {
        return $this->hasMany(WeddingList::class, 'store_id');
    }
    public function wishlist(): HasMany
    {
        return $this->hasMany(Wishlist::class, 'store_id');
    }
}
