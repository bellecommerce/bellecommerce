<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

class StoreProfile extends Model
{
    use HasFactory;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'stores_profile';
    protected $primaryKey = 'id';

    protected $fillable = [
        'phone',
        'whatsapp',
        'cnpj',
        'address',
        'facebook',
        'instagram',
        'youtube',
        'tiktok',
        'logo_path'
    ];


    public function store(): BelongsTo
    {
        return $this->belongsTo(Store::class, 'store_id');
    }
}
