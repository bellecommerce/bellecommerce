<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Customer extends User
{
    use HasFactory;

    protected $table = 'users';

    public function address(): HasMany
    {
        return $this->hasMany(Address::class, 'customer_id');
    }
    public function stores(): BelongsToMany
    {
        return $this->belongsToMany(
            Store::class,
            'customer_stores',
            'customer_id',
            'store_id'
        );
    }
    public function order(): HasMany
    {
        return $this->hasMany(Order::class, 'client_id');
    }

    public function cart(): HasOne
    {
        return $this->hasOne(Cart::class, 'customer_id');
    }
}
