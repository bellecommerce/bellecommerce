<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Observers\MarketPlace;

use App\MarketPlace\ManagerMarketPlace;
use Illuminate\Database\Eloquent\Model;

class MarketPlaceObserver
{
    public function creating(Model $model)
    {
        $marketPlace = app(ManagerMarketPlace::class)->getTenantIdentify();
        $model->setAttribute('market_place_id', $marketPlace);
    }
}
