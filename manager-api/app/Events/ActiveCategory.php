<?php

namespace App\Events;

use App\Services\CategoriesMessageRabbit;
use Exception;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ActiveCategory
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    private object $category;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($category)
    {
        $this->category = $category;
    }

    /**
     * @throws Exception
     */
    public function active()
    {
        return app(CategoriesMessageRabbit::class)->activeCategory($this->category);
    }
}
