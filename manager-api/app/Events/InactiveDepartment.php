<?php

namespace App\Events;

use App\Services\DepartmentMessageRabbit;
use Exception;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class InactiveDepartment
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private int $department;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($department)
    {
        $this->department = $department;
    }

    /**
     * @throws Exception
     */
    public function inactive()
    {
        return app(DepartmentMessageRabbit::class)->inactiveDepartmentMessage($this->department);
    }
}
