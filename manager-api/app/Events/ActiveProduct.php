<?php

namespace App\Events;

use App\Services\ServiceSendMessageRabbit;
use Exception;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ActiveProduct
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private object $product;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($product)
    {
        $this->product = $product;
    }

    /**
     * @throws Exception
     */
    public function active()
    {
        return app(ServiceSendMessageRabbit::class)->publishMessage($this->product);
    }
}
