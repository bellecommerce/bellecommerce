<?php

namespace App\Events;

use App\Services\CategoriesMessageRabbit;
use Exception;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class InactiveCategory
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    private int $category;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($category)
    {
        $this->category = $category;
    }
    /**
     * @throws Exception
     */
    public function inactive()
    {
        return app(CategoriesMessageRabbit::class)->inactiveCategory($this->category);
    }
}
