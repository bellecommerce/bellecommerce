<?php

namespace App\Events;

use App\Services\DepartmentMessageRabbit;
use Exception;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ActiveDepartment
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private object $department;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($department)
    {
        $this->department = $department;
    }

    /**
     * @throws Exception
     */
    public function active()
    {
        return app(DepartmentMessageRabbit::class)->activeDepartment($this->department);
    }
}
