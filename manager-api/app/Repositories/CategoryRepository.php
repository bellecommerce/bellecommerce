<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Repositories;

use App\Interfaces\CategoryRepositoryInterface;
use App\Models\Category;
use App\Models\Store;
use App\Models\SubCategory;

class CategoryRepository implements CategoryRepositoryInterface
{
    protected Category $category;
    protected Store $store;

    public function __construct(Category $category, Store $store)
    {
        $this->category = $category;
        $this->store = $store;
    }

    public function activeInactive($id)
    {
        $category = $this->category->find($id);
        if ($category->status === 0) {
            $category->update(['status' => 1]);
            return $this->category->where('id', $id)->first();
        }
        return $category->update(['status' => 0]);
    }
    public function activeInactiveSubCategory($id)
    {
        $category = SubCategory::find($id);
        if ($category->status === 0) {
            $category->update(['status' => 1]);
            return $this->category->where('id', $id)->first();
        }
        return $category->update(['status' => 0]);
    }

}
