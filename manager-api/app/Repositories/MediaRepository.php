<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Repositories;

use App\Interfaces\MediaRepositoryInterface;
use App\Models\Features;
use App\Models\MediaProducts;
use App\Models\Product;

use Illuminate\Http\Request;

class MediaRepository implements MediaRepositoryInterface
{
    protected Product $product;
    protected MediaProducts $mediaProducts;

    public function __construct(MediaProducts $mediaProducts, Product $product)
    {
        $this->product = $product;
        $this->mediaProducts = $mediaProducts;
    }

    public function all($id): ?object
    {
        $product = Features::find($id);
        return $product->media()->where('feature_product_id', $id)->get();
    }

    public function find(int $id): ?object
    {
        return $this->mediaProducts->find($id);
    }

    public function save($data): ?object
    {
        $product = Features::find($data['feature_product_id']);
        return $product->media()->create($data);
    }

    public function update(int $id, array $attributes): bool
    {
        return $this->user->find($id)->update($attributes);
    }

    public function delete($id): bool
    {
        $image = MediaProducts::find($id);
        return $image->delete();
    }
}
