<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Repositories;

use App\Interfaces\RatingRepositoryInterface;
use App\Models\Product;
use App\Models\User;

class RatingRepository implements RatingRepositoryInterface
{
    protected Product $product;
    protected User $user;

    public function __construct(Product $product, User $user)
    {
        $this->product = $product;
        $this->user = $user;
    }

    public function all(): object
    {
        return $this->store->with('wishlist')->get();
    }

    public function save(array $attributes): object
    {
        $product = $this->product->find($attributes['product_id']);
        return $product->rating()->create($attributes);
    }

    public function find(int $id): object
    {
        return $this->product->with('rating')->find($id);
    }

    public function update(int $id, array $attributes): bool
    {
        return $this->store->find($id)->update($attributes);
    }

    public function delete(int $id): bool
    {
        return $this->store->find($id)->update(['status' => 0]);
    }
}
