<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Repositories;

use App\Interfaces\CustomerAddressRepositoryInterface;
use App\Models\Address;
use App\Models\Customer;

class CustomerAddressRepository implements CustomerAddressRepositoryInterface
{
    protected Customer $customer;

    public function __construct(Customer $customer)
    {
        $this->customer = $customer;
    }

    public function listAll($customer_id): object
    {
        return $this->customer->with('address')
            ->where('id', '=', $customer_id)
            ->get();
    }

    public function save(array $attributes): object
    {
        $customer_id = $attributes['customer_id'];
        $customer = Customer::find($customer_id);
        return $customer->address()->create($attributes);
    }

    public function find(int $id): object
    {
        return Address::find($id);
    }

    public function update(int $id, array $attributes): bool
    {
        $address = Address::find($id);
        return $address->update($attributes);
    }

    public function delete(int $id): bool
    {
        $address = Address::find($id);
        return $address->delete();
    }
}
