<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Repositories;

use App\Interfaces\OrderRepositoryInterface;
use App\Models\Product;
use App\Models\Store;
use App\Models\Order;
use App\Notifications\CreateNewStoreNotification;

class OrderRepository implements OrderRepositoryInterface
{
    protected Store $store;
    protected Order $order;

    public function __construct(Order $order, Store $store)
    {
        $this->order = $order;
        $this->store = $store;
    }

    public function createNumOrder(): ?int
    {
        return Order::select('id')->max('id');
    }

    public function save(array $data): object
    {
        $store = Store::find($data['store_id']);
        return $order = $store->order()->create($data);
    }
    public function relationshipOrderProduct($order, $product_id, $amount, $feature_id)
    {
        $product = Product::find($product_id);
        $order->products()->attach($product, ['amount' => $amount, 'feature_id' => $feature_id]);
    }

    public function find(int $id): object
    {
        return $this->store->with('users')->find($id);
    }

    public function list(): object
    {
        return $this->order->with('products')->get();
    }

    public function update(int $id, array $attributes): bool
    {
        return $this->store->find($id)->update($attributes);
    }

    public function delete(int $id): bool
    {
        return $this->store->find($id)->update(['status' => 0]);
    }
}
