<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Repositories;

use App\Interfaces\ProductRepositoryInterface;
use App\Models\Product;
use App\Models\Store;

class ProductRepository implements ProductRepositoryInterface
{
    protected Store $store;
    protected Product $product;

    public function __construct(Product $product, Store $store)
    {
        $this->store = $store;
        $this->product = $product;
    }

    public function listAll($store_id, $paginate): object
    {
        $store = Store::find($store_id);
        if ($paginate === "null") {
            return $store->product()
                ->orderBy('name', 'ASC')
                ->get();
        }
        return $store->product()
            ->orderBy('id', 'DESC')
            ->paginate(15);
    }

    public function activeProduct($id)
    {
        $product = $this->product->find($id);
        if ($product->status === 0) {
            $product->update(['status' => 1]);
            return $this->product->with(
                'department',
                'categories',
                'subcategories',
                'brand.brand_model',
                'feature'
            )
                ->where('id', '=', $id)
                ->get();
        }
        $product->update(['status' => 0]);
        return $product->id;
    }

    public function save(array $data): object
    {
        $store_id = $data['store_id'];
        $store = Store::find($store_id);
        return $store->product()->create($data);
    }

    public function productRelationshipCategory($product, $category): ?array
    {
        return $product->categories()->attach($category);
    }
    public function productRelationshipSubCategory($product, $category): ?array
    {
        return $product->subcategories()->attach($category);
    }

    public function productUpdateRelationshipCategory($product, $category): ?array
    {
        $product->categories()->detach();
        return $product->categories()->sync($category);
    }
    public function productUpdateRelationshipSubCategory($product, $category): ?array
    {
        $product->subcategories()->detach();
        return $product->subcategories()->sync($category);
    }

    public function saveFeatures($data): object
    {
        $product_id = $data['product_id'];
        $product = Product::find($product_id);
        return $product->feature()->create($data);
    }

    public function find(int $id): ?object
    {
        $result = $this->product->with('department', 'categories', 'subcategories', 'brand.brand_model')
            ->with('feature')
            ->where('id', '=', $id)
            ->get();
        if ($result === null) {
            return null;
        }
        return $result;
    }

    public function update(int $id, array $attributes): bool
    {
        return $this->product->find($id)->update($attributes);
    }

    public function updateFeatures(int $id, array $attributes): bool
    {
        $product = $this->product->find($id);
        return $product->feature()
            ->find($attributes['id'])
            ->update($attributes);
    }

    public function delete(int $id): bool
    {
        return $this->product->find($id)->delete();
    }
}
