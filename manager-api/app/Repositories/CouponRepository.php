<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Repositories;

use App\Interfaces\CouponRepositoryInterface;
use App\Models\Category;
use App\Models\Coupon;
use App\Models\Product;
use App\Models\Store;

class CouponRepository implements CouponRepositoryInterface
{
    protected Store $store;

    public function __construct(Store $store)
    {
        $this->store = $store;
    }

    public function listAll($store_id, $paginate): ?object
    {
        if ($paginate === "null") {
            return Coupon::where('store_id', $store_id)->get();
        }
        return Coupon::where('store_id', $store_id)->paginate(15);
    }

    public function save(array $attributes): ?object
    {
        $store_id = $attributes['store_id'];
        $store = Store::find($store_id);
        return $store->coupon()->create($attributes);
    }

    public function find(int $id): ?object
    {
        return Coupon::with('categories', 'products')->find($id);
    }
    public function search(string $code): ?object
    {
        $result = Coupon::where('code', '=', $code)->first();
        if ($result === null) {
            return null;
        }
        return $result;
    }
    public function update($id, $data): ?bool
    {
        return Coupon::find($id)->update($data);
    }
    public function relationshipCoupon($coupon, $category_id)
    {
        $category = Category::find($category_id);
        $coupon->categories()->attach($category);
    }
    public function relationshipCouponProducts($coupon, $product_id)
    {
        $product = Product::find($product_id);
        $coupon->products()->attach($product);
    }

    public function relationshipCouponUpdate($idCoupon, $category_id)
    {
        $coupon = $this->find($idCoupon);
        $coupon->categories()->sync($category_id);
    }
    public function relationshipCouponProductsUpdate($idCoupon, $product_id)
    {
        $coupon = $this->find($idCoupon);
        $coupon->products()->sync($product_id);
    }
}
