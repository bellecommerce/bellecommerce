<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Repositories;

use App\Interfaces\BrandModelRepositoryInterface;
use App\Models\BrandModel;
use App\Models\Brand;

class BrandModelRepository implements BrandModelRepositoryInterface
{
    protected Brand $brand;
    protected BrandModel $brandModel;

    public function __construct(Brand $brand, BrandModel $brandModel)
    {
        $this->brand = $brand;
        $this->brandModel = $brandModel;
    }

    public function listAll($brand_id): object
    {
        $brand = Brand::find($brand_id);
        return $brand->brand_model;
    }

    public function save(array $attributes): object
    {
        $brand_id = $attributes['brand_id'];
        $brand = Brand::find($brand_id);
        return $brand->brand_model()->create($attributes);
    }

    public function find(int $id): ?object
    {
        $result = $this->brand->with('brand_model')->find($id);
        if ($result === null) {
            return null;
        }
        return $result;
    }

    public function update(int $id, array $attributes): bool
    {
        return $this->product->find($id)->update($attributes);
    }

    public function delete(int $id): bool
    {
        return $this->product->find($id)->delete();
    }
}
