<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Repositories;

use App\Interfaces\PaymentRepositoryInterface;
use App\Models\Store;
use App\Models\Order;

class PaymentRepository implements PaymentRepositoryInterface
{
    protected Store $store;
    protected Order $order;

    public function __construct(Order $order, Store $store)
    {
        $this->order = $order;
        $this->store = $store;
    }

    public function create(array $data): object
    {
        $order = Order::find($data['order_id']);
        return $order->payments()->create($data);
    }
}
