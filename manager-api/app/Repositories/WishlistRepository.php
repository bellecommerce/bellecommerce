<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Repositories;

use App\Interfaces\WishlistRepositoryInterface;
use App\Models\Store;
use App\Models\User;
use App\Models\Wishlist;
use App\Notifications\CreateNewStoreNotification;

class WishlistRepository implements WishlistRepositoryInterface
{
    protected Store $store;
    protected User $user;

    public function __construct(Store $store, User $user)
    {
        $this->store = $store;
        $this->user = $user;
    }

    public function all(): object
    {
        return $this->store->with('wishlist')->get();
    }

    public function save(array $attributes): object
    {
        $store = $this->store->find($attributes['store_id']);
        $wishlist = $store->wishlist()->create($attributes);
        $lists = $attributes['list'];
        foreach ($lists as $list) {
            $wishlist->products()->attach([$list]);
        }
        return $wishlist;
    }

    public function find(int $id): object
    {
        return $this->store->with('user')->find($id);
    }

    public function update(int $id, array $attributes): bool
    {
        return $this->store->find($id)->update($attributes);
    }

    public function delete(int $id): bool
    {
        return $this->store->find($id)->update(['status' => 0]);
    }
}
