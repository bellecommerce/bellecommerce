<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Repositories;

use App\Interfaces\UserRepositoryInterface;
use App\Models\MarketPlace;
use App\Models\User;
use Illuminate\Http\Request;

class UserRepository implements UserRepositoryInterface
{
    protected User $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function getProfile($user): object
    {
        return $this->user->with('profile')->find($user);
    }

    public function find(int $id): object
    {
        return User::with('profile')->find($id);
    }

    public function save(array $attributes): object
    {
        $tenant = MarketPlace::create([
            'name' => $attributes['empresa']
        ]);
        return $tenant->users()->create($attributes);
    }

    public function update(int $id, array $attributes): object
    {
        $profile = $this->user->find($id);
        return $profile->profile()->updateOrCreate(['id' => $attributes['id']], $attributes);
    }

    public function changePhoto(int $id, $data): bool
    {
        return $this->user->find($id)->update($data);
    }

    public function delete($id): bool
    {
        $user = User::find($id);
        return $user->delete();
    }
}
