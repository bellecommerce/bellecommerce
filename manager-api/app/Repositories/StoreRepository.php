<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Repositories;

use App\Interfaces\StoreRepositoryInterface;
use App\Models\Store;
use App\Models\User;
use App\Notifications\CreateNewStoreNotification;

class StoreRepository implements StoreRepositoryInterface
{
    protected Store $store;
    protected User $user;

    public function __construct(Store $store, User $user)
    {
        $this->store = $store;
        $this->user = $user;
    }

    public function all(): object
    {
        return $this->store->with('customers', 'profile')
            ->where('status', '=', '1')
            ->simplePaginate(15);
    }

    public function getStore($user_id): object
    {
        return $this->store->with('profile')
            ->where('user_id', $user_id)
            ->get();
    }

    public function save(array $attributes, $user_id = null): object
    {
        if ($user_id !== null) {
            $user = User::find($user_id);
            $store = $user->shops()->create($attributes);
            $store->profile()->create($attributes['profile']);
            return Store::with('profile')->find($store->id);
        }
        $user = $this->user->create($attributes['user']);
        $store = $user->shops()->create($attributes);
        $store->profile()->create($attributes['profile']);
        if (is_object($store)) {
            $user->notify(new CreateNewStoreNotification($user));
        }
        return $store;
    }

    public function find(int $id): object
    {
        return $this->store->with('user', 'profile')->find($id);
    }

    public function update(int $id, array $attributes): bool
    {
        return $this->store->find($id)->update($attributes);
    }

    public function delete(int $id): bool
    {
        return $this->store->find($id)->update(['status' => 0]);
    }
}
