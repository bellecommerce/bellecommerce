<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Repositories;

use App\Interfaces\SellerRepositoryInterface;
use App\Models\Seller;

class SellerRepository implements SellerRepositoryInterface
{
    protected Seller $seller;

    public function __construct(Seller $seller)
    {
        $this->seller = $seller;
    }

    public function all(): object
    {
        return $this->seller->simplePaginate(15);
    }

    public function find(int $id): object
    {
        return Seller::find($id);
    }

    public function findByColumn(string $column, $value): object
    {
        return $this->seller->where($column, $value)->get();
    }

    public function save(array $attributes): object
    {
        $seller = Seller::create($attributes);
        return $seller->sellerStore()->create(
            [
                'name' => $attributes['store']
            ]
        );
    }

    public function update(int $id, array $attributes): bool
    {
        return $this->seller->find($id)->update($attributes);
    }

    public function delete($id): bool
    {
        $user = Seller::find($id);
        return $user->delete();
    }
}
