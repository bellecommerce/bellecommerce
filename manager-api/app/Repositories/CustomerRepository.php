<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Repositories;

use App\Interfaces\CustomerRepositoryInterface;
use App\Models\Customer;
use App\Models\Store;
use App\Notifications\CreateNewCustomerNotification;

class CustomerRepository implements CustomerRepositoryInterface
{
    protected Store $store;
    protected Customer $customer;

    public function __construct(Customer $customer, Store $store)
    {
        $this->store = $store;
        $this->customer = $customer;
    }

    public function listAll($store_id): object
    {
        $store = Store::find($store_id);
        return $store->customers;
    }

    public function save(array $attributes): object
    {
        $customer = $this->customer->create($attributes);
        if (is_object($customer)) {
            $customer->notify(new CreateNewCustomerNotification($customer));
        }
        $customer->stores()->attach($attributes['store_id']);
        return $customer;
    }

    public function find(int $id): ?object
    {
        return $this->customer->find($id);
    }

    public function update(int $id, array $attributes): bool
    {
        return $this->customer->find($id)->update($attributes);
    }

    public function delete(int $id): bool
    {
        return $this->store->find($id)->update(['status' => 0]);
    }
}
