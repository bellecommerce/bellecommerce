<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Repositories;

use App\Interfaces\BannerRepositoryInterface;
use App\Models\Banner;
use App\Models\Store;
use PhpParser\Node\Expr\Cast\Object_;

class BannerRepository implements BannerRepositoryInterface
{
    protected Store $store;

    public function __construct(Store $store)
    {
        $this->store = $store;
    }

    public function listAll($id): object
    {
        $store = Store::find($id);
        return $store->banners()->get();
    }

    public function save(array $attributes): ?object
    {
        $store_id = $attributes['store_id'];
        $store = Store::find($store_id);
        return $store->banners()->create($attributes);
    }

    public function find(int $id): ?object
    {
        return Banner::find($id);
    }

    public function update(array $attributes): bool
    {
        $banner = Banner::find($attributes['id']);
        return $banner->update($attributes);
    }

    public function delete(int $id): bool
    {
        return $this->product->find($id)->delete();
    }
}
