<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Repositories;

use App\Interfaces\OwnRepositoryInterface;
use App\Models\User;

class OwnRepository implements OwnRepositoryInterface
{
    protected User $ownerMarketPlace;

    public function __construct(User $ownerMarketPlace)
    {
        $this->ownerMarketPlace = $ownerMarketPlace;
    }

    public function all(): object
    {
        //
    }

    public function find(int $id): object
    {
        return User::find($id);
    }

    public function findByColumn(string $column, $value): object
    {
        return $this->ownerMarketPlace->where($column, $value)->get();
    }

    public function save(array $attributes): object
    {
        $owner = User::create($attributes);
        $marketplace = [
            'name' => $attributes['empresa'],
        ];
        return $owner->marketplace()->create($marketplace);
    }

    public function update(int $id, array $attributes): bool
    {
        return $this->ownerMarketPlace->find($id)->update($attributes);
    }

    public function delete($id): bool
    {
        $ownerMarketPlace = User::find($id);
        return $ownerMarketPlace->delete();
    }
}
