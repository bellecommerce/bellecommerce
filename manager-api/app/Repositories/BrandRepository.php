<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Repositories;

use App\Interfaces\BrandRepositoryInterface;
use App\Models\Brand;
use App\Models\Store;

class BrandRepository implements BrandRepositoryInterface
{
    protected Store $store;
    protected Brand $brand;

    public function __construct(Store $store, Brand $brand)
    {
        $this->store = $store;
        $this->brand = $brand;
    }

    public function listAll($store_id): object
    {
        $store = Store::find($store_id);
        return $store->customers;
    }

    public function save(array $attributes): object
    {
        $store_id = $attributes['store_id'];
        $store = Store::find($store_id);
        return $store->brand()->create($attributes);
    }

    public function find(int $id): ?object
    {
        $result = $this->brand->find($id);
        if ($result === null) {
            return null;
        }
        return $result;
    }

    public function update(int $id, array $attributes): bool
    {
        return $this->brand->find($id)->update($attributes);
    }

    public function delete(int $id): bool
    {
        return $this->brand->find($id)->delete();
    }
}
