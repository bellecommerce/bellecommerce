<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Repositories;

use App\Interfaces\DepartmentRepositoryInterface;
use App\Models\Category;
use App\Models\Department;
use App\Models\Store;

class DepartmentRepository implements DepartmentRepositoryInterface
{
    protected Department $department;
    protected Store $store;

    public function __construct(Department $department, Store $store)
    {
        $this->department = $department;
        $this->store = $store;
    }

    public function listAllDepartmentAndCategory(int $id, $paginate): object
    {
        if ($paginate === "null") {
            return $this->department->where('store_id', $id)
                ->with('category')
                ->get();
        }
        return $this->department->where('store_id', $id)
            ->with('category')
            ->paginate(15);
    }

    public function save(int $id, array $attributes): object
    {
        $store = $this->store->find($id);
        return $store->department()->create($attributes);
    }

    public function update(int $id, array $attributes): bool
    {
        return $this->department->find($id)->update($attributes);
    }

    public function createCategory($department, $category)
    {
        return $department->category()->create($category);
    }

    public function updateCategory($department, $category)
    {
        if ($category['id'] === null) {
            return $this->createCategory($department, $category);
        }
        return $department->category->find($category['id'])->update($category);
    }

    public function createSubCategory($category, $subCategory): object
    {
        return $category->subcategory()->create($subCategory);
    }

    public function activeInactive($id)
    {
        $department = $this->department->find($id);
        if ($department->status === 0) {
            $department->update(['status' => 1]);
            return $this->department->where('id', $id)->first();
        }
        return $department->update(['status' => 0]);
    }

    public function updateSubCategory($data, $subCategory)
    {
        $category = Category::find($data['id']);
        if ($subCategory['id'] === null) {
            return $this->createSubCategory($category, $subCategory);
        }
        return $category->subcategory->find($subCategory['id'])->update($subCategory);
    }

    public function find($id): ?object
    {
        return $this->department->with('category')->find($id);
    }

}
