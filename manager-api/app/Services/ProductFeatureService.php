<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Services;

use App\Interfaces\ProductRepositoryInterface;
use App\Models\Product;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\Utils;

class ProductFeatureService
{
    protected ProductRepositoryInterface $productRepository;

    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function createSku(): string
    {
        $maxId = Product::select('id')->max('id');
        return (\Date('Y') . $maxId);
    }

    public function listAll($store_id): object
    {
        return $this->productRepository->listAll($store_id);
    }

    public function create(Request $request): object
    {
        $data = $request->all();
        $data['sku'] = $this->createSku();
        return $this->productRepository->saveFeatures($data);
    }


    public function view($id): ?object
    {
        return $this->productRepository->find($id);
    }

    public function update(Request $request): bool
    {
        $id = $request->input('id');
        return $this->productRepository->update($id, $request->all());
    }

    public function updateFeatures(Request $request): bool
    {
        $id = $request->input('product_id');
        $idFeature = (int) $request->input('id');
        $product = $this->productRepository->find($id);
        foreach ($product as $prod) {
            $feature = $prod['feature'];
            foreach ($feature as $f) {
                if ($idFeature === $f->id) {
                    $result = $this->productRepository->updateFeatures($id, $request->except('_method'));
                }
            }
        }
        return $result;
    }
    public function delete($id): bool
    {
        return $this->productRepository->delete($id);
    }
}
