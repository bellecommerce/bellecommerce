<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Services;

use Exception;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class CategoriesMessageRabbit
{
    private $channel;
    private AMQPStreamConnection $connection;
    public function __construct()
    {
        $this->connection = new AMQPStreamConnection(
            'rabbitmq-bell',
            5672,
            'guest',
            'guest',
            'rabbitmq_local',
        );
        $this->channel = $this->connection->channel();
    }

    public function exchange()
    {
        return $this->channel->exchange_declare(
            'belltech',
            'direct',
            false,
            true,
            false,
            false,
            false,
            false,
            false,
        );
    }

    public function queue($name)
    {
        return $this->channel->queue_declare(
            $name,
            false,
            true,
            false,
            false,
            false,
            false,
            false
        );
    }

    /**
     * @throws Exception
     */
    public function activeCategory($object)
    {
        $response = $this->channel->queue_bind(
            'active_category_queue',
            'belltech',
            'active_category_key'
        );

        $storeNotificationMessage = new AMQPMessage($object);

        $this->channel->basic_publish(
            $storeNotificationMessage,
            'belltech',
            'active_category_key'
        );

        $this->channel->close();
        $this->connection->close();
        return $response;
    }
    public function inactiveCategory($id)
    {
        $response = $this->channel->queue_bind(
            'inactive_category_queue',
            'belltech',
            'inactive_category_key'
        );

        $storeNotificationMessage = new AMQPMessage($id);

        $this->channel->basic_publish(
            $storeNotificationMessage,
            'belltech',
            'inactive_category_key'
        );

        $this->channel->close();
        $this->connection->close();
        return $response;
    }
    public function activeSubCategory($object)
    {
        $response = $this->channel->queue_bind(
            'active_subcategory_queue',
            'belltech',
            'active_subcategory_key'
        );

        $storeNotificationMessage = new AMQPMessage($object);

        $this->channel->basic_publish(
            $storeNotificationMessage,
            'belltech',
            'active_subcategory_key'
        );

        $this->channel->close();
        $this->connection->close();
        return $response;
    }
    public function inactiveSubCategory($id)
    {
        $response = $this->channel->queue_bind(
            'inactive_subcategory_queue',
            'belltech',
            'inactive_subcategory_key'
        );

        $storeNotificationMessage = new AMQPMessage($id);

        $this->channel->basic_publish(
            $storeNotificationMessage,
            'belltech',
            'inactive_subcategory_key'
        );

        $this->channel->close();
        $this->connection->close();
        return $response;
    }
}
