<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Services;

use App\Interfaces\CustomerRepositoryInterface;
use Hash;
use Illuminate\Http\Request;

class CustomerService
{
    protected CustomerRepositoryInterface $customerRepository;
    protected ServiceKeycloak $serviceKeycloak;

    public function __construct(CustomerRepositoryInterface $customerRepository, ServiceKeycloak $serviceKeycloak)
    {
        $this->customerRepository = $customerRepository;
        $this->serviceKeycloak = $serviceKeycloak;
    }

    public function listAll($store_id): object
    {
        return $this->customerRepository->listAll($store_id);
    }

    public function create(Request $request): object
    {
        $data = $request->all();
        $result = $this->serviceKeycloak->createCustomer($data);
        if ($result->getStatusCode() === 201) {
            $userKeycloak = $this->serviceKeycloak->getUserId($request->input('email'));
            $this->serviceKeycloak->sendVerifyEmail($userKeycloak[0]->id);
            $data['password'] = Hash::make($request->input('password'));
            $data['type'] = 'client';
            $data['username'] = $request->input('email');
            return $this->customerRepository->save($data);
        }
    }

    public function view($id): ?object
    {
        return $this->customerRepository->find($id);
    }

    public function update(Request $request): bool
    {
        $id = $request->input('id');
        $data = $request->all();
        $data['username'] = $request->input('email');
        $response = $this->customerRepository->update($id, $data);
        if ($response) {
            $userKeycloak = $this->serviceKeycloak->getUserId($request->input('email'));
            $this->serviceKeycloak->updateCustomer($request->all(), $userKeycloak[0]->id);
        }
        return $response;
    }
    public function delete($id): bool
    {
        return $this->customerRepository->delete($id);
    }
}
