<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Services;

use App\Interfaces\BrandModelRepositoryInterface;
use Illuminate\Http\Request;

class BrandModelService
{
    protected BrandModelRepositoryInterface $brandModelRepository;

    public function __construct(BrandModelRepositoryInterface $brandModelRepository)
    {
        $this->brandModelRepository = $brandModelRepository;
    }

    public function list($brand_id): object
    {
        return $this->brandModelRepository->listAll($brand_id);
    }

    public function create(Request $request): object
    {
        $data = $request->all();
        return $this->brandModelRepository->save($data);
    }

    public function view($id): ?object
    {
        return $this->brandModelRepository->find($id);
    }

    public function update(Request $request): bool
    {
        $id = $request->input('id');
        return $this->brandModelRepository->update($id, $request->all());
    }
    public function delete($id): bool
    {
        return $this->brandModelRepository->delete($id);
    }
}
