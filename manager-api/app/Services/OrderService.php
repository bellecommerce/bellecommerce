<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Services;

use App\Interfaces\OrderRepositoryInterface;
use App\Models\Coupon;
use Illuminate\Http\Request;

class OrderService
{
    protected OrderRepositoryInterface $orderRepository;

    public function __construct(OrderRepositoryInterface $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    public function createNewOrderNumber(): int
    {
        $maxId = $this->orderRepository->createNumOrder();
        return (\Date('Yis') . ($maxId + 1));
    }
    public function create(Request $request): object
    {
        $products = $request->all();
        $total_value = 0.00;
        $products_price = 0.00;
        $discount_price = 0.00;
        foreach ($products['product'] as $product) {
            $total_value += ($product['price'] - $product['discount']) * $product['amount'];
            $products_price += $product['price'];
            $discount_price += $product['discount'];
        }
        if (!empty($products['coupon_code'])) {
            $coupon = Coupon::where('code', '=', $request->input('coupon_code'))->first();
        }
        $data = [
            'order_number'=> $this->createNewOrderNumber(),
            'client_id' => $request->input('customer_id'),
            'store_id' => $request->input('store_id'),
            'coupon_id' => $coupon->id ?? null,
            'shipping_method_id' => null,
            'order_delivery_status_id' => null,
            'shipping_deadline' => null,
            'products_price' => $products_price,
            'discount_price' => $discount_price,
            'shipping_price' => null,
            'total_price' => $total_value,
            'value_fee' => null,
            'status' => 'pending',
            'code_tracking' => null
        ];

        $order = $this->orderRepository->save($data);

        foreach ($products['product'] as $product) {
            $this->orderRepository->relationshipOrderProduct(
                $order,
                $product['id'],
                $product['amount'],
                $product['feature_id']
            );
        }
        return $order;
    }

    public function list(): object
    {
        return $this->orderRepository->list();
    }
}
