<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class ServiceKeycloak
{
    private Client $client;
    private array $header = [];
    public function __construct()
    {
        $this->client = new Client();
        $this->header = [
            "Authorization" => 'Bearer ' . $this->getToken()->access_token,
            "Content-Type" => "application/json"
        ];
    }

    const URL = "http://sso.bellcommerce.com.br:8080/auth/admin/realms/belltech";
    const URL_TOKEN = "http://sso.bellcommerce.com.br:8080/auth/realms/belltech/protocol/openid-connect";
    const URL_VERIFY_EMAIL = "http://sso.bellcommerce.com.br:8080/auth/admin/realms/belltech/users";

    public function getToken()
    {
        try {
            $response = $this->client->post(self::URL_TOKEN . '/token', [
                'form_params' => [
                    "client_secret" => "juS7gOIQT345bNStg4L0nWj89CEst63T",
                    "client_id" => "manager-cli",
                    "grant_type" => "client_credentials"
                ]
            ]);
            return json_decode($response->getBody()->getContents());
        } catch (ClientException $exception) {
            return $exception->getMessage();
        }
    }

    public function createCustomer($user)
    {
        $json = json_encode([
            "enabled" => true,
            "username" => $user['email'],
            "email" => $user['email'],
            "firstName" => $user['name'],
            "lastName" => $user['lastname'],
            "credentials" => [
                [
                    "type" => "password",
                    "value" => $user['password'],
                    "temporary" => false
                ]
            ],
            "requiredActions" => [
                "VERIFY_EMAIL"
            ],
            "groups" => [
                "customer"
            ],
            "realmRoles" => [ "mb-user" ]
        ]);
        try {
            return $this->client->post(self::URL . '/users', [
                'headers' => $this->header,
                'body' => $json
            ]);
        } catch (ClientException $exception) {
            return $exception->getMessage();
        }
    }

    public function updateCustomer($user, $idUser)
    {
        $json = json_encode([
            "id" => $idUser,
            "enabled" => true,
            "username" => $user['email'],
            "email" => $user['email'],
            "firstName" => $user['name'],
            "lastName" => $user['lastname'],
            "credentials" => [
                [
                    "type" => "password",
                    "value" => $user['password'],
                    "temporary" => false
                ]
            ],
        ]);
        try {
            return $this->client->put(self::URL . '/users/' . $idUser, [
                'headers' => $this->header,
                'body' => $json
            ]);
        } catch (ClientException $exception) {
            return $exception->getMessage();
        }
    }

    public function getUserId($email)
    {
        try {
            $user = $this->client->get(self::URL . '/users/?search=' . $email, [
                'headers' => $this->header,
            ]);
            return json_decode($user->getBody()->getContents());
        } catch (ClientException $exception) {
            return $exception->getMessage();
        }
    }

    public function sendVerifyEmail($idUser)
    {
        try {
            return $this->client->put(self::URL_VERIFY_EMAIL . '/' . $idUser . '/send-verify-email', [
                'headers' => $this->header,
            ]);
        } catch (ClientException $exception) {
            return $exception->getMessage();
        }
    }

    public function changePassword($idUser, $newPassword)
    {
        $json = json_encode([
            "type" => "password",
            "value" => $newPassword,
            "temporary" => false
        ]);
        try {
            return $this->client->put(self::URL_VERIFY_EMAIL . '/' . $idUser . '/reset-password', [
                'headers' => $this->header,
                'body' => $json
            ]);
        } catch (ClientException $exception) {
            return $exception->getMessage();
        }
    }
}
