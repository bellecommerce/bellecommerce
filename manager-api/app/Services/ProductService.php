<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Services;

use App\Events\ActiveProduct;
use App\Events\InactiveProduct;
use App\Interfaces\ProductRepositoryInterface;
use App\Jobs\ProductCreate;
use App\Models\Category;
use App\Models\Product;
use App\Models\SubCategory;
use Illuminate\Http\Request;

class ProductService
{
    protected ProductRepositoryInterface $productRepository;

    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function listAll($store_id, $paginate): object
    {
        return $this->productRepository->listAll($store_id, $paginate);
    }

    public function createUpdateCategorySubCategory($product, $request, $action)
    {
        if ($request->input('categories')) {
            if ($action === "create") {
                $this->productRepository->productRelationshipCategory($product, $request->input('categories'));
            } else {
                $this->productRepository->productUpdateRelationshipCategory($product, $request->input('categories'));
            }
        }
        if (!empty($request->input('subcategories'))) {
            if ($action === "create") {
                $this->productRepository->productRelationshipSubCategory($product, $request->input('subcategories'));
            } else {
                $this->productRepository->productUpdateRelationshipSubCategory(
                    $product,
                    $request->input('subcategories')
                );
            }
        } else {
            $this->productRepository->productUpdateRelationshipSubCategory(
                $product,
                $request->input('subcategories')
            );
        }
    }

    public function create(Request $request): ?object
    {
        $data = $request->except('categories', 'subcategories');
        $product = $this->productRepository->save($data);
        $this->createUpdateCategorySubCategory($product, $request, "create");
        return $product;
    }

    public function activeInactiveProduct($id): void
    {
        $product = $this->productRepository->activeProduct($id);
        if (is_object($product)) {
            ActiveProduct::dispatch($product);
        } else {
            InactiveProduct::dispatch($product);
        }
    }

    public function createFeatures(Request $request): ?object
    {
        return $this->productRepository->saveFeatures($request->all());
    }

    public function view($id): ?object
    {
        return $this->productRepository->find($id);
    }

    public function update(Request $request)
    {
        $id = $request->input('id');
        $product = Product::find($id);
        $this->createUpdateCategorySubCategory($product, $request, "update");
        $data = $request->except('categories', 'subcategories', "_method");
        return $this->productRepository->update($id, $data);
    }
    public function delete($id): bool
    {
        return $this->productRepository->delete($id);
    }
}
