<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Services;

use App\Interfaces\SellerRepositoryInterface;
use Hash;
use Illuminate\Http\Request;

class SellerService
{
    protected SellerRepositoryInterface $sellerRepository;

    public function __construct(SellerRepositoryInterface $sellerRepository)
    {
        $this->sellerRepository = $sellerRepository;
    }

    public function listAll(): object
    {
        return $this->sellerRepository->all();
    }

    public function create(Request $request): object
    {
        $data = $request->all();
        $data['password'] = Hash::make($request->input('password'));
        $data['type'] = 'seller';
        return $this->sellerRepository->save($data);
    }

    public function view($id): object
    {
        return $this->sellerRepository->find($id);
    }

    public function update(Request $request): bool
    {
        $id = $request->input('id');
        return $this->sellerRepository->update($id, $request->all());
    }
    public function delete($id): bool
    {
        return $this->sellerRepository->delete($id);
    }

}
