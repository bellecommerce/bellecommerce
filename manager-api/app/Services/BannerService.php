<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Services;

use App\Interfaces\BannerRepositoryInterface;
use App\Models\Banner;
use App\Models\Brand;
use App\Models\Store;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class BannerService
{
    protected BannerRepositoryInterface $bannerRepository;

    public function __construct(BannerRepositoryInterface $bannerRepository)
    {
        $this->bannerRepository = $bannerRepository;
    }

    public function listAll($store_id): object
    {
        return $this->bannerRepository->listAll($store_id);
    }

    public function dataBanner($request)
    {
        return  $data = [
            'name' => $request->input('name'),
            'store_id' => $request->input('store_id'),
            'status' => (string) $request->input('status'),
            'location' => (string) $request->input('location'),
            'expiration' => $request->input('expiration'),
            'link' => $request->input('link'),
            'target' => $request->input('target'),
            'id' => $request->input('id') ?? null
        ];
    }

    public function save(Request $request): ?object
    {
        $data = $this->dataBanner($request);
        $storeObject = Store::find($request->input('store_id'));
        $data['path_image'] = app(AwsS3Service::class)
            ->storeS3($request, $storeObject->name, "images/banners/");
        return $this->bannerRepository->save($data);
    }

    public function view($id): ?object
    {
        return $this->bannerRepository->find($id);
    }

    public function update(Request $request): bool
    {
        $data = $this->dataBanner($request);
        $storeObject = Store::find($request->input('store_id'));
        if ($request->hasFile('image')) {
            $data['path_image'] = app(AwsS3Service::class)
                ->storeS3($request, $storeObject->name, "images/banners/");
        }
        return $this->bannerRepository->update($data);
    }
    public function delete($id): bool
    {
        return $this->bannerRepository->delete($id);
    }
}
