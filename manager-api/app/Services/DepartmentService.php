<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Services;

use App\Events\ActiveDepartment;
use App\Events\InactiveDepartment;
use App\Interfaces\DepartmentRepositoryInterface;
use App\Interfaces\StoreRepositoryInterface;
use Illuminate\Http\Request;

class DepartmentService
{
    protected DepartmentRepositoryInterface $departmentRepository;
    protected StoreRepositoryInterface $storeRepository;

    public function __construct(
        DepartmentRepositoryInterface $departmentRepository,
        StoreRepositoryInterface $storeRepository
    ) {
        $this->departmentRepository = $departmentRepository;
        $this->storeRepository = $storeRepository;
    }

    public function listAllDepartmentAndCategory($idStore, $paginate): object
    {
        return $this->departmentRepository->listAllDepartmentAndCategory($idStore, $paginate);
    }

    public function listAllSubCategories($idStore): object
    {
        return $this->departmentRepository->allSubCategories($idStore);
    }

    public function create(Request $request): object
    {
        $store_id = $request->input('store_id');
        $categories = $request->input('categories');
        $department = $this->departmentRepository->save($store_id, $request->all());
        if (empty($categories)) {
            return $department;
        }
        foreach ($categories as $category) {
            $cat = $this->departmentRepository->createCategory($department, $category);
            if (!empty($category['subcategory'])) {
                foreach ($category['subcategory'] as $subcategory) {
                    $subcategory['department_id'] = $department->id;
                    $subcategory['store_id'] = $store_id;
                    $this->departmentRepository->createSubCategory($cat, $subcategory);
                }
            }
        }
        return $department;
    }

    public function activeInactive($id): void
    {
        $department = $this->departmentRepository->activeInactive($id);
        if (is_object($department)) {
            ActiveDepartment::dispatch($department);
        } else {
            InactiveDepartment::dispatch($id);
        }
    }

    public function view($id): ?object
    {
        return $this->departmentRepository->find($id);
    }

    public function update(Request $request): bool
    {
        $id = $request->input('id');
        $department = $this->view($id);
        $categories = $request->input('categories');
        foreach ($categories as $category) {
            $data = [
                "name" => $category['name'],
                "id" => $category['id'] ?? null,
                "status" => $category['status'],
                "show_menu" => $category['show_menu'],
                "count_products" => $category['count_products'] ?? 0,
                "store_id" => $category['store_id'],
            ];
            $cat = $this->departmentRepository->updateCategory($department, $data);
            if (!empty($category['subcategory'])) {
                foreach ($category['subcategory'] as $subcategory) {
                    $subcategory['category_id'] = $data['id'] ?? $cat->id;
                    $subcategory['department_id'] = $department->id;
                    $subcategory['id'] = $subcategory['id'] ?? null;
                    if ($data['id'] === null) {
                        $this->departmentRepository->updateSubCategory($cat, $subcategory);
                    } else {
                        $this->departmentRepository->updateSubCategory($data, $subcategory);
                    }
                }
            }
        }
        return $this->departmentRepository->update($id, $request->except('categories'));
    }

    public function delete($id): bool
    {
        return $this->departmentRepository->delete($id);
    }
}
