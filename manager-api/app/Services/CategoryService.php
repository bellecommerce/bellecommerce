<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Services;

use App\Events\ActiveCategory;
use App\Events\InactiveCategory;
use App\Interfaces\CategoryRepositoryInterface;
use App\Interfaces\StoreRepositoryInterface;
use Illuminate\Http\Request;

class CategoryService
{
    protected CategoryRepositoryInterface $categoryRepository;
    protected StoreRepositoryInterface $storeRepository;

    public function __construct(
        CategoryRepositoryInterface $categoryRepository,
        StoreRepositoryInterface $storeRepository
    ) {
        $this->categoryRepository = $categoryRepository;
        $this->storeRepository = $storeRepository;
    }

    public function activeInactive(Request $request): void
    {
        $id = $request->input('id');
        $category = $this->categoryRepository->activeInactive($id);
        if (is_object($category)) {
            ActiveCategory::dispatch($category);
        } else {
            InactiveCategory::dispatch($id);
        }
    }
    public function activeInactiveSubCategory(Request $request): void
    {
        $id = $request->input('id');
        $category = $this->categoryRepository->activeInactiveSubCategory($id);
        if (is_object($category)) {
            ActiveCategory::dispatch($category);
        } else {
            InactiveCategory::dispatch($id);
        }
    }
}
