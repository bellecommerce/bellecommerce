<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Services;

use App\Interfaces\CustomerAddressRepositoryInterface;
use Illuminate\Http\Request;

class CustomerAddressService
{
    protected CustomerAddressRepositoryInterface $customerAddressRepository;

    public function __construct(CustomerAddressRepositoryInterface $customerAddressRepository)
    {
        $this->customerAddressRepository = $customerAddressRepository;
    }

    public function listAll($customer_id): object
    {
        return $this->customerAddressRepository->listAll($customer_id);
    }

    public function create(Request $request): object
    {
        $data = $request->all();
        return $this->customerAddressRepository->save($data);
    }

    public function view(int $id): object
    {
        return $this->customerAddressRepository->find($id);
    }

    public function update(Request $request): bool
    {
        $id = $request->input('address_id');
        return $this->customerAddressRepository->update($id, $request->all());
    }
    public function delete($id): bool
    {
        return $this->customerAddressRepository->delete($id);
    }
}
