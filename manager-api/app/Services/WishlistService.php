<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Services;

use App\Interfaces\WishlistRepositoryInterface;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WishlistService
{
    protected WishlistRepositoryInterface $wishlistRepository;

    public function __construct(WishlistRepositoryInterface $wishlistRepository)
    {
        $this->wishlistRepository = $wishlistRepository;
    }

    public function listAll(): object
    {
        return $this->wishlistRepository->all();
    }

    public function create(Request $request): object
    {
        $data = $request->all();

        return $this->wishlistRepository->save($data);
    }

    public function view($id): object
    {
        return $this->wishlistRepository->find($id);
    }

    public function update(Request $request): bool
    {
        $id = $request->input('id');
        return $this->wishlistRepository->update($id, $request->all());
    }
    public function delete($id): bool
    {
        return $this->wishlistRepository->delete($id);
    }
}
