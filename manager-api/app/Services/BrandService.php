<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Services;

use App\Interfaces\BrandRepositoryInterface;
use App\Models\Brand;
use App\Models\Store;
use Illuminate\Http\Request;

class BrandService
{
    protected BrandRepositoryInterface $brandRepository;

    public function __construct(BrandRepositoryInterface $brandRepository)
    {
        $this->brandRepository = $brandRepository;
    }

    public function listAll($store_id): object
    {
        return $this->brandRepository->listAll($store_id);
    }

    public function create(Request $request): object
    {
        $data = $request->all();
        $store_id = $request->input('store_id');
        $store = Store::find($store_id);
        $data['image'] = app(AwsS3Service::class)->storeS3($request, $store->name, "images/brands/");
        return $this->brandRepository->save($data);
    }

    public function view($id): ?object
    {
        return $this->brandRepository->find($id);
    }

    public function update(Request $request): bool
    {
        $id = $request->input('id');
        $data = $request->all();
        if ($request->hasFile('image')) {
            $store_id = $request->input('store_id');
            $store = Store::find($store_id);
            $data['image'] = app(AwsS3Service::class)->storeS3(
                $request,
                $store->name,
                "images/brands/"
            );
        }
        return $this->brandRepository->update($id, $data);
    }
    public function delete($id): bool
    {
        return $this->brandRepository->delete($id);
    }
}
