<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Services;

use PagarmeCoreApiLib\Configuration;
use PagarmeCoreApiLib\PagarmeCoreApiClient;

class PagarmeRequestService
{
    const KEY = 'sk_test_7dmeKddSqIJK1Y3q';

    private PagarmeCoreApiClient $client;
    public function __construct()
    {
        Configuration::$basicAuthUserName = self::KEY;
        Configuration::$basicAuthPassword = '';
        $this->client = new PagarmeCoreApiClient();
    }


    public function creditCard_transaction()
    {

        $orderController = $this->client->getOrders();

        $customer = new \PagarmeCoreApiLib\Models\CreateCustomerRequest();
        $customer->name = "Gustavo V Goulart";
        $customer->email = "avengerstark@ligadajustica.com.br";
        $customer->document = '85444619059';
        $customer->type = 'individual';
        $customer->address = new \PagarmeCoreApiLib\Models\CreateAddressRequest();
        $customer->address->zipCode = '20541380';
        $customer->address->line1 = 'Rua Doutor Aquino, 65';
        $customer->address->complement = '106';
        $customer->address->neighborhood = 'Andaraí';
        $customer->address->city = 'Rio de Janeiro';
        $customer->address->state = 'RJ';
        $customer->address->country = 'BR';

        $customer->phones = new \PagarmeCoreApiLib\Models\CreatePhonesRequest();
        $customer->phones->mobilePhone = new \PagarmeCoreApiLib\Models\CreatePhoneRequest();
        $customer->phones->mobilePhone->countryCode = '55';
        $customer->phones->mobilePhone->areaCode = '21';
        $customer->phones->mobilePhone->number = '996154303';


        $creditCard = new \PagarmeCoreApiLib\Models\CreateCreditCardPaymentRequest();
        $creditCard->capture = true;
        $creditCard->installments = 2;
        $creditCard->card = new \PagarmeCoreApiLib\Models\CreateCardRequest();
        $creditCard->card->number = "4000000000000010";
        $creditCard->card->brand = "VISA";
        $creditCard->card->holderName = "Tony Stark";
        $creditCard->card->expMonth = 1;
        $creditCard->card->expYear = 2025;
        $creditCard->card->cvv = "123";

        $creditCard->card->billingAddress = new \PagarmeCoreApiLib\Models\CreateAddressRequest();
        $creditCard->card->billingAddress->zipCode = '20541380';
        $creditCard->card->billingAddress->line1 = '65, Rua Doutor Aquino';
        $creditCard->card->billingAddress->street = 'Rua Doutor Aquino';
        $creditCard->card->billingAddress->complement = '106';
        $creditCard->card->billingAddress->neighborhood = 'Andaraí';
        $creditCard->card->billingAddress->city = 'Rio de Janeiro';
        $creditCard->card->billingAddress->state = 'RJ';
        $creditCard->card->billingAddress->country = 'BR';

        $request = new \PagarmeCoreApiLib\Models\CreateOrderRequest();

        $request->items = [new \PagarmeCoreApiLib\Models\CreateOrderItemRequest()];
        $request->items[0]->code = '1873';
        $request->items[0]->description = "Tesseract Bracelet";
        $request->items[0]->quantity = 1;
        $request->items[0]->amount = 23490; // this value should be in cents

        $request->payments = [new \PagarmeCoreApiLib\Models\CreatePaymentRequest()];
        $request->payments[0]->paymentMethod = "credit_card";
        $request->payments[0]->creditCard = $creditCard;
        $request->customer = $customer;

        //dd(json_encode($request, JSON_PRETTY_PRINT));
        $result = $orderController->createOrder($request);

        dd(json_encode($result, JSON_PRETTY_PRINT));
    }
}
