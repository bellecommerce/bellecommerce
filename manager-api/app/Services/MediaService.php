<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Services;

use App\Interfaces\MediaRepositoryInterface;
use App\Models\Features;
use App\Models\Product;
use App\Models\Store;
use App\Repositories\MediaRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class MediaService
{
    protected MediaRepositoryInterface $mediaRepository;

    public function __construct(MediaRepositoryInterface $mediaRepository)
    {
        $this->mediaRepository = $mediaRepository;
    }

    public function listAll($id): ?object
    {
        return $this->mediaRepository->all($id);
    }

    public function newNameImage(): string
    {
        return strtolower(substr(md5(date("YmdHis")), 1, 15));
    }

    public function create(Request $request): ?array
    {
        $feature_id = $request->get('feature_product_id');
        $product_id = Features::select('product_id')->where('id', $feature_id)->first();
        $store = Product::with('store')->where('id', $product_id->product_id)->first();
        $dir = ("images/products/" . str_replace(' ', '_', $store->store->name));
        foreach ($request->file('image') as $image) {
            $s3 = $image->store($dir);
            $fullPath = ('https://bellecommerce.s3.amazonaws.com/' . $s3);
            $data = [
                'media_path' => $fullPath,
                'feature_product_id' => $request->input('feature_product_id')
            ];
            $response[] = $this->mediaRepository->save($data);
        }
        if ($response) {
            return $response;
        }
    }

    public function view($id): ?object
    {
        return $this->mediaRepository->find($id);
    }

    public function update(Request $request): bool
    {
        $id = $request->input('id');
        return $this->userRepository->update($id, $request->all());
    }
    public function delete($id): bool
    {
        $media = $this->mediaRepository->find($id);
        $path = substr($media->media_path, 39);
        if ($media->media_path) {
            Storage::disk('s3')->delete($path);
        }
        return $this->mediaRepository->delete($id);
    }

    public function imageDescription($request): string
    {
        $storeObject = Store::find($request->input('store_id'));
        return app(AwsS3Service::class)
                ->storeS3($request, $storeObject->name, "images/products/");
    }
}
