<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Services;

use Exception;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use phpDocumentor\Reflection\DocBlock\Tags\Throws;

class ServiceSendMessageRabbit
{
    private $channel;
    private AMQPStreamConnection $connection;
    public function __construct()
    {
        $this->connection = new AMQPStreamConnection(
            'rabbitmq-bell',
            5672,
            'guest',
            'guest',
            'rabbitmq_local',
        );
        $this->channel = $this->connection->channel();
    }

    public function exchange()
    {
        return $this->channel->exchange_declare(
            'belltech',
            'direct',
            false,
            true,
            false,
            false,
            false,
            false,
            false,
        );
    }

    public function queue($name)
    {
        return $this->channel->queue_declare(
            $name,
            false,
            true,
            false,
            false,
            false,
            false,
            false
        );
    }

    /**
     * @throws Exception
     */
    public function publishMessage($object)
    {
        $response = $this->channel->queue_bind(
            'active_product_queue',
            'belltech',
            'active_product_key'
        );

        $storeNotificationMessage = new AMQPMessage($object);

        $this->channel->basic_publish(
            $storeNotificationMessage,
            'belltech',
            'active_product_key'
        );

        $this->channel->close();
        $this->connection->close();
        return $response;
    }
    public function inactiveProductMessage($id)
    {
        $response = $this->channel->queue_bind(
            'inactive_product_queue',
            'belltech',
            'inactive_product_key'
        );

        $storeNotificationMessage = new AMQPMessage($id);

        $this->channel->basic_publish(
            $storeNotificationMessage,
            'belltech',
            'inactive_product_key'
        );

        $this->channel->close();
        $this->connection->close();
        return $response;
    }
}
