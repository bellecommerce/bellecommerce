<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Services;

use App\Models\Order;

use jlcd\Cielo\Resources\CieloPayment;
use jlcd\Cielo\Resources\CieloCreditCard;
use jlcd\Cielo\Resources\CieloCustomer;
use jlcd\Cielo\Resources\CieloOrder;
use Config;
class CieloRequestService
{
    const clientId = "fccf3bd1-e927-4638-91e0-57bc3c5581aa";
    const clientSecret = "MWVYDTSUTBIOPIDJCCYFWPSMUUAUMIJOVBTVDPCJ";

    private Order $order;
    private string $customer;
    private int $value;
    private string $orderNumber;

    public function __construct(Order $order)
    {
      $this->order = $order;
    }

    public function getOrderAndCustomer($order_id)
    {
        Config::set('cielo.merchant_id', self::clientId);
        Config::set('cielo.merchant_key', self::clientSecret);
        $orderStore = $this->order->with('customer')->find($order_id);
        //dd($orderStore);

        $this->value = (int) ($orderStore->total_price * 100);
        $this->orderNumber = (string) $orderStore->order_number;
        $this->customer = $orderStore->customer->name;

        //dd($this->value, $this->orderNumber, $this->customer);

    }

    public function creditCardPayment($cardNumber, $expirationDate, $brand, $securityCode, $holder, $order_id)
    {
        $this->getOrderAndCustomer($order_id);
        $payment = new CieloPayment();
        $payment->setValue($this->value);

        $creditCard = new CieloCreditCard();
        $creditCard->setCardNumber($cardNumber);
        $creditCard->setExpirationDate($expirationDate);
        $creditCard->setBrand($brand);
        $creditCard->setSecurityCode($securityCode);
        $creditCard->setHolder($holder);

        $payment->setCreditCard($creditCard);

        $order = new CieloOrder();
        $order->setId($this->orderNumber);

        $customer = new CieloCustomer();
        $customer->setName($this->customer);

        $result = app()->cielo->payment($payment, $order, $customer);

        dd($result);
    }

    public function creditCardConsult($order_id)
    {
        $this->getOrderAndCustomer($order_id);
        $payment = new CieloPayment();
        $payment->setId($this->orderNumber);
        $payment->setValue($this->value);

        $payment = app()->cielo->capturePayment($payment);
        dd($payment);
    }

}
