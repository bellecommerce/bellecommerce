<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Services;

use App\Interfaces\UserRepositoryInterface;
use App\Models\Store;
use App\Repositories\UserRepository;
use Hash;
use Illuminate\Http\Request;

class UserService
{
    protected UserRepositoryInterface $userRepository;
    protected ServiceKeycloak $serviceKeycloak;

    public function __construct(UserRepositoryInterface $userRepository, ServiceKeycloak $serviceKeycloak)
    {
        $this->userRepository = $userRepository;
        $this->serviceKeycloak = $serviceKeycloak;
    }

    public function getProfile($user): object
    {
        return $this->userRepository->getProfile($user);
    }

    public function create(Request $request): object
    {
        $data = $request->all();
        $data['password'] = Hash::make($request->input('password'));
        $data['username'] = $request->input('email');
        return $this->userRepository->save($data);
    }

    public function view($id): object
    {
        return $this->userRepository->find($id);
    }

    public function update(Request $request): object
    {
        $id = \Auth::id();
        foreach ($request->all('profile') as $profile) {
            $data = [
                "id" => $profile['id'] ?? null,
                "cpf_cnpj" => $profile['cpf_cnpj'],
                "rg" => $profile['rg'],
                "birthday" => $profile['birthday'],
                "gender" => $profile['gender'],
                "facebook" => $profile['facebook'],
                "instagram" => $profile['instagram'],
                "tiktok" => $profile['tiktok'],
                "whatsapp" => $profile['whatsapp'],
                "mobile_phone" => $profile['mobile_phone'],
            ];
        }
        return $this->userRepository->update($id, $data);
    }
    public function changePhoto($request): bool
    {
        $store_id = $request->input('store_id');
        $user_id = \Auth::id();
        $store = Store::find($store_id);
        $data['profile_photo_path'] = app(AwsS3Service::class)
            ->storeS3(
                $request,
                $store->name,
                "images/users/"
            );
        return $this->userRepository->changePhoto($user_id, $data);
    }

    public function changePassword(Request $request): bool
    {
        $userKeycloakId = $this->serviceKeycloak->getUserId($request->input('email'));
        $response = $this->serviceKeycloak->changePassword($userKeycloakId[0]->id, $request->input('newPassword'));
        if ($response->getStatusCode() === 204) {
            return true;
        }
    }
}
