<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Services;

use App\Interfaces\PaymentRepositoryInterface;
use App\Models\Order;
use App\Notifications\CreateNewPaymentNotification;
use Hash;
use Illuminate\Http\Request;

class PaymentService
{
    protected PaymentRepositoryInterface $paymentRepository;
    protected CieloRequestService $cieloRequestService;
    protected PagarmeRequestService $pagarmeRequestService;

    public function __construct(
        PaymentRepositoryInterface $paymentRepository,
        CieloRequestService $cieloRequestService,
        PagarmeRequestService $pagarmeRequestService
    ) {
        $this->paymentRepository = $paymentRepository;
        $this->cieloRequestService = $cieloRequestService;
        $this->pagarmeRequestService = $pagarmeRequestService;
    }

    public function save(Request $request): object
    {
        $data = $request->all();
        $this->notificationPayment($data['order_id']);
        $result = $this->paymentRepository->create($data);
        if (!empty($result)) {
            return $this->pagarmeRequestService->creditCard_transaction();
            /*return $this->cieloRequestService->creditCardPayment(
                $data['cardNumber'],
                $data['expirationDate'],
                $data['card_brand'],
                $data['securityCode'],
                $data['holder'],
                $data['order_id']
            );*/
        }
    }
    public function notificationPayment($order_id)
    {
        $order = Order::with('store', 'customer')->find($order_id);
        $dataUser = [
            'subject' => 'Venda realizada',
            'line' => 'Uma venda foi realizada pela loja' . ' ' . ($order->store->name)
        ];
        $dataCustomer = [
            'subject' => 'Confirmação de compra',
            'line' => 'Seu pagamento esta em analise. Número:' . ' ' . ($order->order_number)
        ];

        $user = $order->store->users;
        $customer = $order->customer;
        $customer->notify(new CreateNewPaymentNotification($customer, $dataCustomer));
        $user->notify(new CreateNewPaymentNotification($user, $dataUser));
    }

    public function consult(Request $request)
    {
        $data = $request->all();
        //$result = $this->paymentRepository->create($data);
        if (!empty($result)) {
            return $this->cieloRequestService->creditCardConsult(
                $data['order_id']
            );
        }
    }
}
