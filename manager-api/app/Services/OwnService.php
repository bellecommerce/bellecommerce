<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Services;

use App\Interfaces\OwnRepositoryInterface;
use App\Repositories\OwnRepository;
use Hash;
use Illuminate\Http\Request;

class OwnService
{
    protected OwnRepositoryInterface $ownRepository;

    public function __construct(OwnRepositoryInterface $ownRepository)
    {
        $this->ownRepository = $ownRepository;
    }

    public function listAll(): object
    {
        return $this->ownRepository->all();
    }

    public function create(Request $request): object
    {
        $data = $request->all();
        $data['password'] = Hash::make($request->input('password'));
        $data['type'] = 'owner';
        return $this->ownRepository->save($data);
    }

    public function view($id): object
    {
        return $this->ownRepository->find($id);
    }

    public function update(Request $request): bool
    {
        $id = $request->input('id');
        return $this->ownRepository->update($id, $request->all());
    }
    public function delete($id): bool
    {
        return $this->ownRepository->delete($id);
    }

}
