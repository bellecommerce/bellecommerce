<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Services;

use App\Interfaces\WeddingListRepositoryInterface;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WeddingListService
{
    protected WeddingListRepositoryInterface $weddingListRepository;

    public function __construct(WeddingListRepositoryInterface $weddingListRepository)
    {
        $this->weddingListRepository = $weddingListRepository;
    }

    public function listAll(): object
    {
        return $this->weddingListRepository->all();
    }

    public function create(Request $request): object
    {
        $data = $request->all();

        return $this->weddingListRepository->save($data);
    }

    public function view($id): object
    {
        return $this->weddingListRepository->find($id);
    }

    public function update(Request $request): bool
    {
        $id = $request->input('id');
        return $this->weddingListRepository->update($id, $request->all());
    }
    public function delete($id): bool
    {
        return $this->weddingListRepository->delete($id);
    }
}
