<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Services;

class AwsS3Service
{
    public function storeS3($request, $storeName, $bucket)
    {
        $store = str_replace(" ", '_', $storeName);
        $dir = ($bucket . str_replace(' ', '_', $store));

        if ($request->hasFile('image')) {
            $s3 = $request->image->store($dir);
            $fullPath = ('https://bellecommerce.s3.amazonaws.com/' . $s3);
        }
        return $fullPath;
    }
}
