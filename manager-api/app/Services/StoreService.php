<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Services;

use App\Interfaces\StoreRepositoryInterface;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StoreService
{
    protected StoreRepositoryInterface $storeRepository;
    protected ServiceSendMessageRabbit $messageRabbit;

    public function __construct(StoreRepositoryInterface $storeRepository, ServiceSendMessageRabbit $messageRabbit)
    {
        $this->storeRepository = $storeRepository;
        $this->messageRabbit = $messageRabbit;
    }

    public function listAll(): object
    {
        return $this->storeRepository->all();
    }

    public function getStore($user_id): object
    {
        return $this->storeRepository->getStore($user_id);
    }

    public function create(Request $request): object
    {
        if ($request->hasFile('logo_path')) {
            $dir = ("images/logo/" . str_replace(' ', '_', $request->input('name')));
            $s3 = $request->logo_path->store($dir);
            $fullPath = ('https://bellecommerce.s3.amazonaws.com/' . $s3);
            $data['image'] = $fullPath;
        }
        $data = [
            'name' => $request->input('storeName'),
            'is_marketplace' => $request->input('is_marketplace'),
            'iframe_google_maps' => $request->input('iframe_google_maps'),
            'status' => $request->input('status'),
            'domain' => $request->input('domain'),
            'subdomain' => $request->input('subdomain'),
        ];
        $data['profile'] = [
            'facebook' => $request->input('facebook'),
            'instagram' => $request->input('instagram'),
            'youtube' => $request->input('youtube'),
            'tiktok' => $request->input('tiktok'),
            'phone' => $request->input('phone'),
            'whatsapp' => $request->input('whatsapp'),
            'cnpj' => $request->input('cnpj'),
            'address' => $request->input('address'),
            'logo_path' => $fullPath ?? null,
        ];
        $data['user'] = [
            'password' => Hash::make($request->input('user.password')),
            'type' => 'ownerStore',
            'email' => $request->input('email'),
            'username' => $request->input('email'),
            'name' => $request->input('name'),
            'lastname' => $request->input('lastname'),
            'cpf' => $request->input('cpf'),
        ];

        if ($request->input('user_id')) {
            $store = $this->storeRepository->save($data, $request->input('user_id'));
        } else {
            $store = $this->storeRepository->save($data);
        }

        $store = $this->storeRepository->find($store->id);
        $this->messageRabbit->sendStoreToNotification($store);
        $this->messageRabbit->createStore($store);
        return $store;
    }

    public function view($id): object
    {
        return $this->storeRepository->find($id);
    }

    public function update(Request $request): bool
    {
        $id = $request->input('id');
        return $this->storeRepository->update($id, $request->all());
    }
    public function delete($id): bool
    {
        return $this->storeRepository->delete($id);
    }
}
