<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Services;

use App\Interfaces\RatingRepositoryInterface;
use App\Models\Product;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RatingService
{
    protected RatingRepositoryInterface $ratingRepository;

    public function __construct(RatingRepositoryInterface $ratingRepository)
    {
        $this->ratingRepository = $ratingRepository;
    }

    public function listAll(): object
    {
        return $this->ratingRepository->all();
    }

    public function create(Request $request): object
    {
        $data = $request->all();
        return $this->ratingRepository->save($data);
    }

    public function view($id)
    {
        $result = $this->ratingRepository->find($id);
        $count = 0;
        $stars = 0;
        $comments = [];
        foreach ($result->rating as $rating) {
            $count += 1;
            $stars += $rating->stars;
            $comments[] = $rating->comments;
        }
        $total = ($stars / $count);
        return $data = [
            'stars' => (int) $total,
            'comments' => $comments
        ];
    }

    public function update(Request $request): bool
    {
        $id = $request->input('id');
        return $this->ratingRepository->update($id, $request->all());
    }
    public function delete($id): bool
    {
        return $this->ratingRepository->delete($id);
    }
}
