<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Services;

use App\Interfaces\CouponRepositoryInterface;
use App\Models\Coupon;
use Illuminate\Http\Request;

class CouponService
{
    protected CouponRepositoryInterface $couponRepository;

    public function __construct(CouponRepositoryInterface $couponRepository)
    {
        $this->couponRepository = $couponRepository;
    }

    public function listAll($store_id, $paginate): ?object
    {
        return $this->couponRepository->listAll($store_id, $paginate);
    }

    public function couponData($request)
    {
        return $data = [
            "code" => $request->input('code'),
            "percentage" => $request->input('percentage'),
            "amount" => $request->input('amount'),
            "expiration" => $request->input('expiration'),
            "ship" => $request->input('ship'),
            "product" => $request->input('product'),
            "category" => $request->input('category'),
            "description" => $request->input('description'),
            "store_id" => $request->input('store_id'),
            "status" => $request->input('status'),
            "quantity" => $request->input('quantity'),
        ];
    }
    public function create(Request $request): ?object
    {
        $data = $this->couponData($request);
        $coupon = $this->couponRepository->save($data);

        if ($request->input('category')) {
            foreach ($request->input('categories') as $category) {
                $this->couponRepository->relationshipCoupon($coupon, $category);
            }
        }
        if ($request->input('product')) {
            foreach ($request->input('products') as $product) {
                $this->couponRepository->relationshipCouponProducts($coupon, $product);
            }
        }

        return $coupon;
    }

    public function view($id): ?object
    {
        return $this->couponRepository->find($id);
    }
    public function search($code): ?object
    {
        return $this->couponRepository->search($code);
    }

    public function update(Request $request): ?bool
    {
        $data = $this->couponData($request);
        $id = $request->input('id');
        $coupon = $this->couponRepository->update($id, $data);
        $category = $request->input('category');
        $product = $request->input('product');
        if ($category !== "false") {
            foreach ($request->input('categories') as $category) {
                $categories[] = $category;
            }
            $this->couponRepository->relationshipCouponUpdate($id, $categories);
        }
        if ($product !== "false") {
            foreach ($request->input('products') as $product) {
                $products[] = $product;
            }
            $this->couponRepository->relationshipCouponProductsUpdate($id, $products);
        }
        return $coupon;
    }
    public function delete($id): bool
    {
        return $this->couponRepository->delete($id);
    }
}
