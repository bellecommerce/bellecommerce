<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Scopes\MarketPlace;

use App\MarketPlace\ManagerMarketPlace;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class MarketPlaceScope implements Scope
{
    public function apply(Builder $builder, Model $model)
    {
        $tenant = app(ManagerMarketPlace::class)->getStoreIdentify();
        $builder->where('market_place_id', $tenant);
    }
}
