<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\MarketPlace\Traits;

use App\Observers\MarketPlace\MarketPlaceObserver;
use App\Scopes\MarketPlace\MarketPlaceScope;
use Ramsey\Uuid\Uuid;

trait TenantTraitStore
{
    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->store_code = (string) Uuid::uuid4();
        });
        //static::addGlobalScope(new MarketPlaceScope());
        static::observe(new MarketPlaceObserver());
    }
}
