<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\MarketPlace\Traits;

use App\Observers\MarketPlace\MarketPlaceObserver;

trait TenantTraitUsers
{
    public static function boot()
    {
        parent::boot();
        static::observe(new MarketPlaceObserver());
    }
}
