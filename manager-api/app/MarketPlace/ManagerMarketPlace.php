<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\MarketPlace;

use App\Models\MarketPlace;
use App\Models\Store;
use Illuminate\Support\Facades\Auth;

class ManagerMarketPlace
{
    public function getTenantIdentify()
    {
        return Auth::user()->marketplace->id;
    }

    public function getStoreIdentify(): \Illuminate\Database\Eloquent\Collection
    {
        return Auth::user()->shops->id;
    }

    public function subdomain()
    {
        $subdomain = explode('.', request()->getHost());
        return $subdomain[0];
    }

    public function domain(): string
    {
        return request()->getHost();
    }
    public function tenant()
    {
        $domain = $this->subdomain();
        $tenantDomain = Store::where('subdomain', '=', $domain . '.belltech.com')->first();
        if ($tenantDomain === null) {
            return 'api.bell.com';
        } else {
            return $tenantDomain;
        }
    }
}
