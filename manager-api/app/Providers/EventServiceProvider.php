<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Providers;

use App\Events\ActiveCategory;
use App\Events\ActiveDepartment;
use App\Events\ActiveProduct;
use App\Events\InactiveCategory;
use App\Events\InactiveDepartment;
use App\Events\InactiveProduct;
use App\Listeners\ActiveCategoryListener;
use App\Listeners\ActiveDepartmentListener;
use App\Listeners\ActiveProductListener;
use App\Listeners\InactiveCategoryListener;
use App\Listeners\InactiveDepartmentListener;
use App\Listeners\InactiveProductListener;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        ActiveProduct::class => [
            ActiveProductListener::class,
        ],
        InactiveProduct::class => [
            InactiveProductListener::class,
        ],
        ActiveDepartment::class => [
            ActiveDepartmentListener::class,
        ],
        InactiveDepartment::class => [
            InactiveDepartmentListener::class
        ],
        ActiveCategory::class => [
            ActiveCategoryListener::class,
        ],
        InactiveCategory::class => [
            InactiveCategoryListener::class
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
