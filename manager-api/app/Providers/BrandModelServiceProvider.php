<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Providers;

use App\Interfaces\BrandModelRepositoryInterface;
use App\Models\BrandModel;
use App\Models\Brand;
use App\Repositories\BrandModelRepository;
use Illuminate\Support\ServiceProvider;

class BrandModelServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(BrandModelRepositoryInterface::class, function ($app) {
            return new BrandModelRepository(new Brand(), new BrandModel());
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
