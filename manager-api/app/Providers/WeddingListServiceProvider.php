<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Providers;

use App\Interfaces\WeddingListRepositoryInterface;
use App\Models\Store;
use App\Models\User;
use App\Repositories\WeddingListRepository;
use Illuminate\Support\ServiceProvider;

class WeddingListServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(WeddingListRepositoryInterface::class, function ($app) {
            return new WeddingListRepository(new Store(), new User());
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
