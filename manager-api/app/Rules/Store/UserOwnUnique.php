<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

namespace App\Rules\Store;

use App\MarketPlace\ManagerMarketPlace;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\DB;

class UserOwnUnique implements Rule
{
    private string $table;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($table)
    {
        $this->table = $table;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $tenant = app(ManagerMarketPlace::class)->getTenantIdentify();
        $result = DB::table($this->table)->with('users', 'market_places')
                    ->where($attribute, $value)
                    ->where('tenant_id', $tenant)
                    ->first();

        return is_null($result);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The validation error message.';
    }
}
