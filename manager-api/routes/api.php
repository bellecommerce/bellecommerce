<?php
/*
 * *
 *  @author Dayvison Silva - Diretor de Desenvolvimento - <dayvisonsilva@gmail.com>
 *  @author Gustavo V. Goulart - Desenvolvedor Sênior - <gustavo@inovacto.com.br>
 *
 *  @copyright © 2022 Bell Tecnologia. Todos os direitos reservados.
 * /
 */

use App\Http\Controllers\Api\Auth\LoginController;
use App\Http\Controllers\Api\Banner\BannerController;
use App\Http\Controllers\Api\Brand\BrandController;
use App\Http\Controllers\Api\BrandModel\BrandModelController;
use App\Http\Controllers\Api\Cart\CartController;
use App\Http\Controllers\Api\Category\CategoryController;
use App\Http\Controllers\Api\Category\SubCategoryController;
use App\Http\Controllers\Api\Contracts\ContractController;
use App\Http\Controllers\Api\Coupon\CouponController;
use App\Http\Controllers\Api\Customer\CustomerAddressController;
use App\Http\Controllers\Api\Customer\CustomerController;
use App\Http\Controllers\Api\Department\DepartmentController;
use App\Http\Controllers\Api\Media\MediaController;
use App\Http\Controllers\Api\Order\OrderController;
use App\Http\Controllers\Api\Payment\PaymentController;
use App\Http\Controllers\Api\Product\ColorController;
use App\Http\Controllers\Api\Product\OtherFeatureController;
use App\Http\Controllers\Api\Product\ProductController;
use App\Http\Controllers\Api\Product\ProductFeatureController;
use App\Http\Controllers\Api\Product\SizeController;
use App\Http\Controllers\Api\Product\TechnicalDataController;
use App\Http\Controllers\Api\Rating\RatingController;
use App\Http\Controllers\Api\Seller\SellerController;
use App\Http\Controllers\Api\Store\StoreController;
use App\Http\Controllers\Api\MarketPlace\OwnerController;
use App\Http\Controllers\Api\Users\UserController;
use App\Http\Controllers\Api\WeddingList\WeddingListController;
use App\Http\Controllers\Api\Wishlist\WishlistController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->group(function () {
    Route::post('/create', [OwnerController::class, 'create']);
});

Route::middleware(['auth:api', 'throttle'])->group(function () {
        Route::group(['prefix' => 'v1', 'namespace' => 'Api'], function () {
            Route::prefix('marketplace')->namespace('MarketPlace')->group(function () {
                Route::post('/index', [OwnerController::class, 'index']);
                Route::post('/create', [OwnerController::class, 'create']);
            });
            Route::prefix('users')->namespace('Users')->group(function () {
                Route::get('/profile', [UserController::class, 'profile']);
                Route::post('/create', [UserController::class, 'create']);
                Route::get('/profile/view', [UserController::class, 'view']);
                Route::put('/profile/update', [UserController::class, 'update']);
                Route::patch('/changePhoto', [UserController::class, 'changePhoto']);
                Route::patch('/changePassword', [UserController::class, 'changePassword']);
            });
            Route::prefix('store')->namespace('Store')->group(function () {
                Route::get('/list', [StoreController::class, 'index']);
                Route::post('/create', [StoreController::class, 'create']);
                Route::get('/view/{id}', [StoreController::class, 'view']);
                Route::put('/update', [StoreController::class, 'update']);
                Route::get('/delete/{id}', [StoreController::class, 'delete']);
                Route::get('/getStore', [StoreController::class, 'getStore']);
            });
            Route::prefix('banner')->namespace('Banner')->group(function () {
                Route::get('/index/{id}', [BannerController::class, 'index']);
                Route::post('/create', [BannerController::class, 'save']);
                Route::put('/update', [BannerController::class, 'update']);
                Route::get('/view/{id}', [BannerController::class, 'view']);
            });
            Route::prefix('department')->namespace('Department')->group(function () {
                Route::post('/create', [DepartmentController::class, 'create']);
                Route::get('/index/{id}/{paginate}', [DepartmentController::class, 'index']);
                Route::get('/view/{id}', [DepartmentController::class, 'view']);
                Route::get('/activeInactive/{id}', [DepartmentController::class, 'activeInactive']);
                Route::put('/update', [DepartmentController::class, 'update']);
            });
            Route::prefix('category')->namespace('Category')->group(function () {
                Route::post('/activeInactive', [CategoryController::class, 'activeInactive']);
            });
            Route::prefix('subcategory')->namespace('Category')->group(function () {
                Route::post('/activeInactive', [SubCategoryController::class, 'activeInactive']);
            });
            Route::prefix('product')->namespace('Product')->group(function () {
                Route::post('/create', [ProductController::class, 'create']);
                Route::get('/index/{id}/{paginate}', [ProductController::class, 'index']);
                Route::put('/update', [ProductController::class, 'update']);
                Route::get('/view/{id}', [ProductController::class, 'view']);
                Route::get('/delete/{id}', [ProductController::class, 'delete']);
                Route::get('/activeInactiveProduct/{id}', [ProductController::class, 'activeInactiveProduct']);

                Route::get('/color/index/{id}', [ColorController::class, 'index']);
                Route::post('/color/create', [ColorController::class, 'store']);
                Route::get('/color/show/{id}', [ColorController::class, 'show']);
                Route::post('/color/update/{id}', [ColorController::class, 'update']);
                Route::get('/color/destroy/{id}', [ColorController::class, 'destroy']);

                Route::get('/size/index/{id}', [SizeController::class, 'index']);
                Route::post('/size/create', [SizeController::class, 'store']);
                Route::get('/size/show/{id}', [SizeController::class, 'show']);
                Route::post('/size/update/{id}', [SizeController::class, 'update']);
                Route::get('/size/destroy/{id}', [SizeController::class, 'destroy']);

                Route::get('/technicalFeature/index/{id}', [TechnicalDataController::class, 'index']);
                Route::post('/technicalFeature/create', [TechnicalDataController::class, 'store']);

                Route::get('/otherFeature/index/{id}', [OtherFeatureController::class, 'index']);
                Route::post('/otherFeature/create', [OtherFeatureController::class, 'store']);

                Route::get('/productFeature/index/{id}', [ProductFeatureController::class, 'index']);
                Route::post('/productFeature/create', [ProductFeatureController::class, 'store']);
                Route::put('/productFeature/update', [ProductFeatureController::class, 'update']);
                Route::namespace('Rating')->group(function () {
                    Route::post('/rating', [RatingController::class, 'create']);
                    Route::get('/showRating/{id}', [RatingController::class, 'show']);
                });
            });
            Route::prefix('media')->namespace('Media')->group(function () {
                Route::post('/create', [MediaController::class, 'create']);
                Route::post('/imageDescription', [MediaController::class, 'imageDescription']);
                Route::get('/index/{id}', [MediaController::class, 'index']);
                Route::delete('/delete/{id}', [MediaController::class, 'delete']);
            });
            Route::prefix('brand')->namespace('Brand')->group(function () {
                Route::post('/create', [BrandController::class, 'create']);
                Route::get('/list/{id}', [BrandController::class, 'list']);
                Route::get('/view/{id}', [BrandController::class, 'view']);
                Route::put('/update', [BrandController::class, 'update']);
            });
            Route::prefix('coupon')->namespace('Coupon')->group(function () {
                Route::post('/create', [CouponController::class, 'create']);
                Route::get('/index/{id}/{paginate}', [CouponController::class, 'index']);
                Route::get('/search/{id}', [CouponController::class, 'search']);
                Route::get('/view/{id}', [CouponController::class, 'view']);
                Route::post('/update', [CouponController::class, 'update']);
            });
            Route::prefix('brandModel')->namespace('BrandModel')->group(function () {
                Route::post('/create', [BrandModelController::class, 'create']);
                Route::get('/list/{id}', [BrandModelController::class, 'list']);
                Route::get('/view/{id}', [BrandModelController::class, 'view']);
            });
            Route::prefix('order')->namespace('Order')->group(function () {
                Route::post('/create', [OrderController::class, 'create']);
                Route::get('/list', [OrderController::class, 'list']);
            });
            Route::prefix('payment')->namespace('Payment')->group(function () {
                Route::post('/create', [PaymentController::class, 'create']);
                Route::post('/consult', [PaymentController::class, 'consult']);
            });
            Route::prefix('customer')->namespace('Customer')->group(function () {
                Route::post('/create', [CustomerController::class, 'create']);
                Route::get('/index/{id}', [CustomerController::class, 'index']);
                Route::get('/view/{id}', [CustomerController::class, 'view']);
                Route::put('/update', [CustomerController::class, 'update']);
                Route::post('/newAddress', [CustomerAddressController::class, 'newAddress']);
                Route::post('/updateAddress', [CustomerAddressController::class, 'updateAddress']);
                Route::get('/listAddress/{id}', [CustomerAddressController::class, 'listAddress']);
                Route::get('/viewAddress/{id}', [CustomerAddressController::class, 'viewAddress']);
                Route::get('/deleteAddress/{id}', [CustomerAddressController::class, 'deleteAddress']);
            });

            Route::prefix('contract')->namespace('Contract')->group(function () {
                Route::post('/index', [ContractController::class, 'index']);
                Route::post('/create', [ContractController::class, 'create']);
                Route::get('/show/{id}', [ContractController::class, 'show']);
                Route::post('/update', [ContractController::class, 'update']);
                Route::post('/attach', [ContractController::class, 'attach']);
            });
            Route::prefix('seller')->namespace('Seller')->group(function () {
                Route::post('/create', [SellerController::class, 'create']);
            });
            Route::prefix('wedding')->namespace('WeddingList')->group(function () {
                Route::get('/index', [WeddingListController::class, 'index']);
                Route::post('/create', [WeddingListController::class, 'create']);
            });
            Route::prefix('wishlist')->namespace('Wishlist')->group(function () {
                Route::get('/index', [WishlistController::class, 'index']);
                Route::post('/create', [WishlistController::class, 'create']);
            });
            Route::prefix('cart')->namespace('Cart')->group(function () {
                Route::get('/index', [CartController::class, 'index']);
                Route::get('/add/{id}', [CartController::class, 'create']);
                Route::get('/delete/{id}', [CartController::class, 'destroy']);
            });
        });
});
