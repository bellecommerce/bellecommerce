-- MySQL dump 10.13  Distrib 8.0.28, for Linux (x86_64)
--
-- Host: localhost    Database: mktplace
-- ------------------------------------------------------
-- Server version	8.0.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `address`
--

DROP TABLE IF EXISTS `address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `address` (
  `id` int NOT NULL AUTO_INCREMENT,
  `cep` varchar(10) NOT NULL,
  `street` varchar(100) NOT NULL,
  `state` varchar(50) NOT NULL,
  `uf` char(4) NOT NULL,
  `city` varchar(100) NOT NULL,
  `neighborhood` varchar(100) DEFAULT NULL,
  `number` int NOT NULL,
  `complement` varchar(50) DEFAULT NULL,
  `customer_id` bigint unsigned NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `address_customer_id_fk` (`customer_id`),
  CONSTRAINT `address_users_id_fk` FOREIGN KEY (`customer_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address`
--

LOCK TABLES `address` WRITE;
/*!40000 ALTER TABLE `address` DISABLE KEYS */;
/*!40000 ALTER TABLE `address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `audits`
--

DROP TABLE IF EXISTS `audits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `audits` (
  `id` varchar(36) NOT NULL,
  `event` varchar(255) NOT NULL,
  `model` varchar(255) NOT NULL,
  `entity_id` varchar(36) NOT NULL,
  `request_id` varchar(36) NOT NULL,
  `json_object` text NOT NULL,
  `description` text,
  `source_id` varchar(255) DEFAULT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `audits`
--

LOCK TABLES `audits` WRITE;
/*!40000 ALTER TABLE `audits` DISABLE KEYS */;
/*!40000 ALTER TABLE `audits` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `banners`
--

DROP TABLE IF EXISTS `banners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `banners` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `path_image` varchar(255) NOT NULL,
  `store_id` bigint unsigned NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `banners_stores_id_fk` (`store_id`),
  CONSTRAINT `banners_stores_id_fk` FOREIGN KEY (`store_id`) REFERENCES `stores` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banners`
--

LOCK TABLES `banners` WRITE;
/*!40000 ALTER TABLE `banners` DISABLE KEYS */;
INSERT INTO `banners` VALUES (1,'banner principal','images/Flamengo_Store/banner principal.jpg',36,'2022-03-15 02:55:40','2022-03-15 02:55:40'),(2,'banner secundario','images/Flamengo_Store/banner_secundario.jpg',36,'2022-03-15 02:57:46','2022-03-15 02:57:46'),(3,'banner secundario','images/Dog_Store/banner_secundario.jpg',35,'2022-03-15 15:27:18','2022-03-15 15:27:18');
/*!40000 ALTER TABLE `banners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `brand_model`
--

DROP TABLE IF EXISTS `brand_model`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `brand_model` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `brand_id` bigint unsigned NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `model_brands_id_fk` (`brand_id`),
  CONSTRAINT `model_brands_id_fk` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `brand_model`
--

LOCK TABLES `brand_model` WRITE;
/*!40000 ALTER TABLE `brand_model` DISABLE KEYS */;
INSERT INTO `brand_model` VALUES (6,'Camisa de jogo','Description of Model',7,'2022-03-14 03:35:10','2022-03-14 03:35:10'),(7,'BRAS&RYT','Description of Model',8,'2022-03-15 12:58:05','2022-03-15 12:58:05');
/*!40000 ALTER TABLE `brand_model` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `brands`
--

DROP TABLE IF EXISTS `brands`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `brands` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` varchar(200) NOT NULL,
  `store_id` bigint unsigned NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `brands_stores_id_fk` (`store_id`),
  CONSTRAINT `brands_stores_id_fk` FOREIGN KEY (`store_id`) REFERENCES `stores` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `brands`
--

LOCK TABLES `brands` WRITE;
/*!40000 ALTER TABLE `brands` DISABLE KEYS */;
INSERT INTO `brands` VALUES (7,'Adidas','Patrocinador Oficial do Flamengo',36,'2022-03-14 03:00:04','2022-03-14 03:00:04'),(8,'Brastemp','Marca lider',36,'2022-03-15 12:57:33','2022-03-15 12:57:33');
/*!40000 ALTER TABLE `brands` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cart`
--

DROP TABLE IF EXISTS `cart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cart` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `product_id` bigint unsigned NOT NULL,
  `customer_id` bigint unsigned DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cart_products_id_fk` (`product_id`),
  KEY `cart_users_id_fk` (`customer_id`),
  CONSTRAINT `cart_products_id_fk` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
  CONSTRAINT `cart_users_id_fk` FOREIGN KEY (`customer_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cart`
--

LOCK TABLES `cart` WRITE;
/*!40000 ALTER TABLE `cart` DISABLE KEYS */;
/*!40000 ALTER TABLE `cart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `categories` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` bigint unsigned DEFAULT NULL,
  `store_id` bigint unsigned NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `status` int DEFAULT '1',
  `show_menu` int DEFAULT '0',
  `count_products` int DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `categories_stores_id_fk` (`store_id`),
  KEY `categories_categories_id_fk` (`parent_id`),
  CONSTRAINT `categories_categories_id_fk` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`),
  CONSTRAINT `categories_stores_id_fk` FOREIGN KEY (`store_id`) REFERENCES `stores` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (13,NULL,36,'Camisa',1,1,3,'2022-03-14 02:59:02','2022-03-14 02:59:02'),(14,NULL,36,'Eletrodomestico',1,1,3,'2022-03-15 12:58:36','2022-03-15 12:58:36'),(15,14,36,'Geladeira',1,1,8,'2022-03-15 13:04:24','2022-03-15 13:04:24');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `colors`
--

DROP TABLE IF EXISTS `colors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `colors` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `store_id` bigint unsigned NOT NULL,
  `color_name` varchar(100) NOT NULL,
  `rgb` varchar(50) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `color_stores_id_fk` (`store_id`),
  CONSTRAINT `color_stores_id_fk` FOREIGN KEY (`store_id`) REFERENCES `stores` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `colors`
--

LOCK TABLES `colors` WRITE;
/*!40000 ALTER TABLE `colors` DISABLE KEYS */;
INSERT INTO `colors` VALUES (4,36,'Vermelho','#F0F0F0',NULL,'2022-03-14 03:23:50','2022-03-14 03:23:50'),(5,36,'Branco','#FFFFFF',NULL,'2022-03-15 14:02:05','2022-03-15 14:02:05');
/*!40000 ALTER TABLE `colors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contract`
--

DROP TABLE IF EXISTS `contract`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `contract` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `tag` enum('marketplace','store') NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contract`
--

LOCK TABLES `contract` WRITE;
/*!40000 ALTER TABLE `contract` DISABLE KEYS */;
INSERT INTO `contract` VALUES (5,'loja simples','Contratos especifico para stores','store','2022-03-13 14:23:07','2022-03-13 14:23:07');
/*!40000 ALTER TABLE `contract` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contract_condition`
--

DROP TABLE IF EXISTS `contract_condition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `contract_condition` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `contract_id` int NOT NULL,
  `min_value` decimal(8,2) DEFAULT NULL,
  `max_value` decimal(10,2) DEFAULT NULL,
  `percent` varchar(10) NOT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `contract_condition_contract_id_fk` (`contract_id`),
  CONSTRAINT `contract_condition_contract_id_fk` FOREIGN KEY (`contract_id`) REFERENCES `contract` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contract_condition`
--

LOCK TABLES `contract_condition` WRITE;
/*!40000 ALTER TABLE `contract_condition` DISABLE KEYS */;
INSERT INTO `contract_condition` VALUES (9,5,0.00,1000.00,'5%',100.00,1,'2022-03-13 14:23:07','2022-03-13 14:23:07'),(10,5,1000.00,5000.00,'10%',200.00,1,'2022-03-13 14:23:07','2022-03-13 14:23:07'),(11,5,5000.00,10000.00,'15%',300.00,1,'2022-03-13 14:23:07','2022-03-13 14:23:07'),(12,5,10000.00,15000.00,'20%',400.00,1,'2022-03-13 14:23:07','2022-03-13 14:23:07');
/*!40000 ALTER TABLE `contract_condition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `coupon_category`
--

DROP TABLE IF EXISTS `coupon_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `coupon_category` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `coupon_id` bigint unsigned DEFAULT NULL,
  `category_id` bigint unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `coupon_category_coupons_id_fk` (`coupon_id`),
  KEY `coupon_category_categories_id_fk` (`category_id`),
  CONSTRAINT `coupon_category_categories_id_fk` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`),
  CONSTRAINT `coupon_category_coupons_id_fk` FOREIGN KEY (`coupon_id`) REFERENCES `coupons` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `coupon_category`
--

LOCK TABLES `coupon_category` WRITE;
/*!40000 ALTER TABLE `coupon_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `coupon_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `coupons`
--

DROP TABLE IF EXISTS `coupons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `coupons` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `store_id` bigint unsigned DEFAULT NULL,
  `code` varchar(255) NOT NULL,
  `percentage` int DEFAULT NULL,
  `amount` decimal(10,2) NOT NULL,
  `expiration` datetime NOT NULL,
  `shipping` tinyint(1) DEFAULT '0',
  `product` tinyint(1) DEFAULT '0',
  `description` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `coupons_stores_id_fk` (`store_id`),
  CONSTRAINT `coupons_stores_id_fk` FOREIGN KEY (`store_id`) REFERENCES `stores` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `coupons`
--

LOCK TABLES `coupons` WRITE;
/*!40000 ALTER TABLE `coupons` DISABLE KEYS */;
/*!40000 ALTER TABLE `coupons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_stores`
--

DROP TABLE IF EXISTS `customer_stores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `customer_stores` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `customer_id` bigint unsigned DEFAULT NULL,
  `store_id` bigint unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `client_stores_clients_id_fk` (`customer_id`),
  KEY `client_stores_stores_id_fk` (`store_id`),
  CONSTRAINT `client_stores_stores_id_fk` FOREIGN KEY (`store_id`) REFERENCES `stores` (`id`),
  CONSTRAINT `customer_stores_users_id_fk` FOREIGN KEY (`customer_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_stores`
--

LOCK TABLES `customer_stores` WRITE;
/*!40000 ALTER TABLE `customer_stores` DISABLE KEYS */;
INSERT INTO `customer_stores` VALUES (12,51,36);
/*!40000 ALTER TABLE `customer_stores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `failed_jobs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `features`
--

DROP TABLE IF EXISTS `features`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `features` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `model_id` bigint DEFAULT NULL,
  `product_id` bigint unsigned DEFAULT NULL,
  `size_id` bigint unsigned DEFAULT NULL,
  `color_id` bigint unsigned DEFAULT NULL,
  `technical_data_id` bigint unsigned DEFAULT NULL,
  `other_feature_id` bigint unsigned DEFAULT NULL,
  `sku` varchar(100) DEFAULT NULL,
  `weight` decimal(8,2) DEFAULT NULL,
  `height` decimal(8,2) DEFAULT NULL,
  `width` decimal(8,2) DEFAULT NULL,
  `length` decimal(8,2) DEFAULT NULL,
  `feature_stock` int DEFAULT NULL,
  `is_feature_plus_sale` int DEFAULT NULL,
  `feature_bar_code` varchar(100) DEFAULT NULL,
  `feature_price` decimal(10,2) DEFAULT NULL,
  `feature_discount_price` decimal(10,2) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `products_variations_brand_model_id_fk` (`model_id`),
  KEY `features_color_id_fk` (`color_id`),
  KEY `features_size_id_fk` (`size_id`),
  KEY `features_technical_data_id_fk` (`technical_data_id`),
  KEY `features_other_feature_id_fk` (`other_feature_id`),
  CONSTRAINT `features_color_id_fk` FOREIGN KEY (`color_id`) REFERENCES `colors` (`id`),
  CONSTRAINT `features_other_feature_id_fk` FOREIGN KEY (`other_feature_id`) REFERENCES `other_feature` (`id`),
  CONSTRAINT `features_size_id_fk` FOREIGN KEY (`size_id`) REFERENCES `size` (`id`),
  CONSTRAINT `features_technical_data_id_fk` FOREIGN KEY (`technical_data_id`) REFERENCES `technical_data` (`id`),
  CONSTRAINT `products_variations_brand_model_id_fk` FOREIGN KEY (`model_id`) REFERENCES `brand_model` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `features`
--

LOCK TABLES `features` WRITE;
/*!40000 ALTER TABLE `features` DISABLE KEYS */;
INSERT INTO `features` VALUES (17,6,19,4,4,NULL,NULL,'202219',2.00,20.00,30.00,20.00,100,0,'98677655544434333',254.00,0.00,'2022-03-14 03:35:21','2022-03-14 03:35:21'),(18,7,20,NULL,5,NULL,NULL,'202220',35.00,20.00,30.00,20.00,10,0,'98677655544434333',2754.00,0.00,'2022-03-15 14:04:16','2022-03-15 14:04:16');
/*!40000 ALTER TABLE `features` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `market_places`
--

DROP TABLE IF EXISTS `market_places`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `market_places` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `owner_id` bigint unsigned DEFAULT NULL,
  `name` varchar(191) NOT NULL,
  `uuid` char(36) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `market_places_users_id_fk` (`owner_id`),
  CONSTRAINT `market_places_users_id_fk` FOREIGN KEY (`owner_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `market_places`
--

LOCK TABLES `market_places` WRITE;
/*!40000 ALTER TABLE `market_places` DISABLE KEYS */;
INSERT INTO `market_places` VALUES (9,38,'Bell Tech','1f7c2047-d7b6-4e33-920e-ed56b67cf005','2022-03-11 06:20:48','2022-03-11 06:20:48');
/*!40000 ALTER TABLE `market_places` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media_products`
--

DROP TABLE IF EXISTS `media_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `media_products` (
  `id` int NOT NULL AUTO_INCREMENT,
  `product_id` bigint unsigned NOT NULL,
  `media_path` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `media_products_products_id_fk` (`product_id`),
  CONSTRAINT `media_products_products_id_fk` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media_products`
--

LOCK TABLES `media_products` WRITE;
/*!40000 ALTER TABLE `media_products` DISABLE KEYS */;
/*!40000 ALTER TABLE `media_products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_products`
--

DROP TABLE IF EXISTS `order_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `order_products` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `product_id` bigint unsigned NOT NULL,
  `feature_id` bigint unsigned DEFAULT NULL,
  `order_id` bigint unsigned NOT NULL,
  `amount` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_products_products_id_fk` (`product_id`),
  KEY `order_products_orders_id_fk` (`order_id`),
  CONSTRAINT `order_products_orders_id_fk` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`),
  CONSTRAINT `order_products_products_id_fk` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_products`
--

LOCK TABLES `order_products` WRITE;
/*!40000 ALTER TABLE `order_products` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `orders` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `order_number` int NOT NULL,
  `store_id` bigint unsigned NOT NULL,
  `client_id` bigint unsigned NOT NULL,
  `coupon_id` bigint unsigned DEFAULT NULL,
  `shipping_method_id` int DEFAULT NULL,
  `order_delivery_status_id` int DEFAULT NULL,
  `shipping_deadline` int DEFAULT NULL,
  `products_price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `discount_price` decimal(10,2) DEFAULT '0.00',
  `shipping_price` decimal(10,2) DEFAULT '0.00',
  `total_price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `value_fee` decimal(10,2) DEFAULT '0.00',
  `status` enum('pending','refused','paid','cancel') NOT NULL,
  `code_tracking` varchar(145) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `orders_clients_id_fk` (`client_id`),
  KEY `orders_coupons_id_fk` (`coupon_id`),
  KEY `orders_stores_id_fk` (`store_id`),
  CONSTRAINT `orders_clients_id_fk` FOREIGN KEY (`client_id`) REFERENCES `users` (`id`),
  CONSTRAINT `orders_coupons_id_fk` FOREIGN KEY (`coupon_id`) REFERENCES `coupons` (`id`),
  CONSTRAINT `orders_stores_id_fk` FOREIGN KEY (`store_id`) REFERENCES `stores` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `other_feature`
--

DROP TABLE IF EXISTS `other_feature`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `other_feature` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `store_id` bigint unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `other_feature_stores_id_fk` (`store_id`),
  CONSTRAINT `other_feature_stores_id_fk` FOREIGN KEY (`store_id`) REFERENCES `stores` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `other_feature`
--

LOCK TABLES `other_feature` WRITE;
/*!40000 ALTER TABLE `other_feature` DISABLE KEYS */;
/*!40000 ALTER TABLE `other_feature` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `other_feature_item`
--

DROP TABLE IF EXISTS `other_feature_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `other_feature_item` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `other_feature_id` bigint unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `description` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `other_feature_item_other_feature_id_fk` (`other_feature_id`),
  CONSTRAINT `other_feature_item_other_feature_id_fk` FOREIGN KEY (`other_feature_id`) REFERENCES `other_feature` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `other_feature_item`
--

LOCK TABLES `other_feature_item` WRITE;
/*!40000 ALTER TABLE `other_feature_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `other_feature_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payments`
--

DROP TABLE IF EXISTS `payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `payments` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `order_id` bigint unsigned NOT NULL,
  `store_id` bigint unsigned NOT NULL,
  `type_payment` varchar(30) DEFAULT NULL,
  `card_brand` varchar(30) DEFAULT NULL,
  `status` enum('pending','paid','refused','cancel') NOT NULL,
  `numberOrder` int DEFAULT NULL,
  `installments` int DEFAULT NULL,
  `url_boleto` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `payments_orders_id_fk` (`order_id`),
  KEY `payments_stores_id_fk` (`store_id`),
  CONSTRAINT `payments_orders_id_fk` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`),
  CONSTRAINT `payments_stores_id_fk` FOREIGN KEY (`store_id`) REFERENCES `stores` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=156 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payments`
--

LOCK TABLES `payments` WRITE;
/*!40000 ALTER TABLE `payments` DISABLE KEYS */;
/*!40000 ALTER TABLE `payments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personal_access_tokens`
--

DROP TABLE IF EXISTS `personal_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `personal_access_tokens` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint unsigned NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personal_access_tokens`
--

LOCK TABLES `personal_access_tokens` WRITE;
/*!40000 ALTER TABLE `personal_access_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `personal_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_rating`
--

DROP TABLE IF EXISTS `product_rating`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product_rating` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `product_id` bigint unsigned DEFAULT NULL,
  `customer_id` bigint unsigned DEFAULT NULL,
  `stars` int DEFAULT NULL,
  `comments` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_rating_products_id_fk` (`product_id`),
  KEY `product_rating_users_id_fk` (`customer_id`),
  CONSTRAINT `product_rating_products_id_fk` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
  CONSTRAINT `product_rating_users_id_fk` FOREIGN KEY (`customer_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_rating`
--

LOCK TABLES `product_rating` WRITE;
/*!40000 ALTER TABLE `product_rating` DISABLE KEYS */;
INSERT INTO `product_rating` VALUES (2,19,51,3,'Lorem Ipsum is simply dummy text of the printing and typesetting industry.','2022-03-16 19:53:33','2022-03-16 19:53:33'),(3,19,51,3,'Lorem Ipsum is simply dummy text of the printing and typesetting industry.','2022-03-16 19:55:00','2022-03-16 19:55:00'),(4,19,51,4,'Lorem Ipsum is simply dummy text of the printing and typesetting industry.','2022-03-16 19:55:31','2022-03-16 19:55:31'),(5,19,51,4,'Lorem Ipsum is simply dummy text of the printing and typesetting industry.','2022-03-16 19:57:15','2022-03-16 19:57:15'),(6,19,51,4,'Lorem Ipsum is simply dummy text of the printing and typesetting industry.','2022-03-16 22:04:15','2022-03-16 22:04:15'),(7,19,51,5,'Lorem Ipsum is simply dummy text of the printing and typesetting industry.','2022-03-16 22:04:25','2022-03-16 22:04:25'),(8,19,51,5,'Lorem Ipsum is simply dummy text of the printing and typesetting industry.','2022-03-16 22:04:27','2022-03-16 22:04:27'),(9,19,51,5,'Lorem Ipsum is simply dummy text of the printing and typesetting industry.','2022-03-16 22:04:29','2022-03-16 22:04:29'),(10,19,51,4,'Lorem Ipsum is simply dummy text of the printing and typesetting industry.','2022-03-16 22:04:37','2022-03-16 22:04:37'),(11,19,51,4,'Lorem Ipsum is simply dummy text of the printing and typesetting industry.','2022-03-17 04:42:11','2022-03-17 04:42:11'),(12,19,51,4,'Lorem Ipsum is simply dummy text of the printing and typesetting industry.','2022-03-17 04:42:13','2022-03-17 04:42:13'),(13,19,51,4,'Lorem Ipsum is simply dummy text of the printing and typesetting industry.','2022-03-17 04:42:14','2022-03-17 04:42:14'),(14,19,51,4,'Lorem Ipsum is simply dummy text of the printing and typesetting industry.','2022-03-17 04:42:14','2022-03-17 04:42:14'),(15,19,51,4,'Lorem Ipsum is simply dummy text of the printing and typesetting industry.','2022-03-17 04:42:15','2022-03-17 04:42:15'),(16,19,51,2,'Lorem Ipsum is simply dummy text of the printing and typesetting industry.','2022-03-17 04:42:29','2022-03-17 04:42:29'),(17,19,51,2,'Lorem Ipsum is simply dummy text of the printing and typesetting industry.','2022-03-17 04:42:29','2022-03-17 04:42:29'),(18,19,51,2,'Lorem Ipsum is simply dummy text of the printing and typesetting industry.','2022-03-17 04:42:30','2022-03-17 04:42:30'),(19,19,51,2,'Lorem Ipsum is simply dummy text of the printing and typesetting industry.','2022-03-17 04:42:30','2022-03-17 04:42:30'),(20,19,51,2,'Lorem Ipsum is simply dummy text of the printing and typesetting industry.','2022-03-17 04:42:30','2022-03-17 04:42:30'),(21,19,51,2,'Lorem Ipsum is simply dummy text of the printing and typesetting industry.','2022-03-17 04:42:31','2022-03-17 04:42:31'),(22,19,51,2,'Lorem Ipsum is simply dummy text of the printing and typesetting industry.','2022-03-17 04:42:31','2022-03-17 04:42:31'),(23,19,51,2,'Lorem Ipsum is simply dummy text of the printing and typesetting industry.','2022-03-17 04:42:31','2022-03-17 04:42:31'),(24,19,51,2,'Lorem Ipsum is simply dummy text of the printing and typesetting industry.','2022-03-17 04:42:32','2022-03-17 04:42:32'),(25,19,51,2,'Lorem Ipsum is simply dummy text of the printing and typesetting industry.','2022-03-17 04:42:32','2022-03-17 04:42:32'),(26,19,51,2,'Lorem Ipsum is simply dummy text of the printing and typesetting industry.','2022-03-17 04:42:33','2022-03-17 04:42:33'),(27,19,51,2,'Lorem Ipsum is simply dummy text of the printing and typesetting industry.','2022-03-17 04:42:33','2022-03-17 04:42:33'),(28,19,51,2,'Lorem Ipsum is simply dummy text of the printing and typesetting industry.','2022-03-17 04:42:33','2022-03-17 04:42:33'),(29,19,51,2,'Lorem Ipsum is simply dummy text of the printing and typesetting industry.','2022-03-17 04:42:33','2022-03-17 04:42:33'),(30,19,51,2,'Lorem Ipsum is simply dummy text of the printing and typesetting industry.','2022-03-17 04:42:34','2022-03-17 04:42:34'),(31,19,51,2,'Lorem Ipsum is simply dummy text of the printing and typesetting industry.','2022-03-17 04:42:34','2022-03-17 04:42:34'),(32,19,51,5,'Lorem Ipsum is simply dummy text of the printing and typesetting industry.','2022-03-17 04:42:53','2022-03-17 04:42:53');
/*!40000 ALTER TABLE `product_rating` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `products` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `store_id` bigint unsigned NOT NULL,
  `category_id` bigint unsigned NOT NULL,
  `ref` varchar(100) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `is_start` smallint DEFAULT '0',
  `status` smallint DEFAULT NULL,
  `meta_google_title` text,
  `meta_google_description` text,
  `meta_google_keywords` text,
  `send_google_merchant` int DEFAULT '1',
  `warranty` varchar(100) DEFAULT NULL,
  `bar_code` varchar(100) DEFAULT NULL,
  `brand_id` bigint unsigned NOT NULL,
  `is_plus_sale` smallint DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `products_stores_id_fk` (`store_id`),
  KEY `products_categories_id_fk` (`category_id`),
  KEY `products_brands_id_fk` (`brand_id`),
  CONSTRAINT `products_brands_id_fk` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`),
  CONSTRAINT `products_categories_id_fk` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`),
  CONSTRAINT `products_stores_id_fk` FOREIGN KEY (`store_id`) REFERENCES `stores` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (19,36,13,'fla8181','Camisa do Flamengo','Camisa nova do Flamengo com todos os patchs.',1,1,NULL,NULL,NULL,NULL,'1 ano','98347659498576954875487349',7,1,'2022-03-14 03:00:26','2022-03-14 03:00:26'),(20,36,15,'BRASYTR','Geladeira Brastemp','Camisa nova do Flamengo com todos os patchs.',1,1,NULL,NULL,NULL,NULL,'1 ano','98347659498576954875487349',8,1,'2022-03-15 14:01:28','2022-03-15 14:01:28'),(21,40,15,'BRASYTR','Geladeira Brastemp','Camisa nova do Flamengo com todos os patchs.',1,1,NULL,NULL,NULL,NULL,'1 ano','98347659498576954875487349',8,1,'2022-03-25 14:27:01','2022-03-25 14:27:01');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sellers_store`
--

DROP TABLE IF EXISTS `sellers_store`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sellers_store` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `seller_id` bigint unsigned DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sellers_store_users_id_fk` (`seller_id`),
  CONSTRAINT `sellers_store_users_id_fk` FOREIGN KEY (`seller_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sellers_store`
--

LOCK TABLES `sellers_store` WRITE;
/*!40000 ALTER TABLE `sellers_store` DISABLE KEYS */;
INSERT INTO `sellers_store` VALUES (10,'Nação',46,'2022-03-11 21:31:15','2022-03-11 21:31:15');
/*!40000 ALTER TABLE `sellers_store` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shipping`
--

DROP TABLE IF EXISTS `shipping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `shipping` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `shipping_method_id` int DEFAULT NULL,
  `order_id` bigint unsigned DEFAULT NULL,
  `customer_id` bigint unsigned DEFAULT NULL,
  `origin` varchar(255) DEFAULT NULL,
  `destination` varchar(255) DEFAULT NULL,
  `amount` decimal(8,2) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shipping`
--

LOCK TABLES `shipping` WRITE;
/*!40000 ALTER TABLE `shipping` DISABLE KEYS */;
/*!40000 ALTER TABLE `shipping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shipping_method`
--

DROP TABLE IF EXISTS `shipping_method`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `shipping_method` (
  `id` int NOT NULL AUTO_INCREMENT,
  `shipping_method` enum('transportadora','correios') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shipping_method`
--

LOCK TABLES `shipping_method` WRITE;
/*!40000 ALTER TABLE `shipping_method` DISABLE KEYS */;
INSERT INTO `shipping_method` VALUES (5,'correios');
/*!40000 ALTER TABLE `shipping_method` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `size`
--

DROP TABLE IF EXISTS `size`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `size` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `size` varchar(50) NOT NULL,
  `store_id` bigint unsigned NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `size_stores_id_fk` (`store_id`),
  CONSTRAINT `size_stores_id_fk` FOREIGN KEY (`store_id`) REFERENCES `stores` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `size`
--

LOCK TABLES `size` WRITE;
/*!40000 ALTER TABLE `size` DISABLE KEYS */;
INSERT INTO `size` VALUES (4,'G',36,'2022-03-14 03:21:57','2022-03-14 03:21:57');
/*!40000 ALTER TABLE `size` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `store_contract`
--

DROP TABLE IF EXISTS `store_contract`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `store_contract` (
  `id` int NOT NULL AUTO_INCREMENT,
  `store_id` bigint unsigned DEFAULT NULL,
  `contract_id` int DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `store_contract_contract_id_fk` (`contract_id`),
  KEY `store_contract_stores_id_fk` (`store_id`),
  CONSTRAINT `store_contract_contract_id_fk` FOREIGN KEY (`contract_id`) REFERENCES `contract` (`id`),
  CONSTRAINT `store_contract_stores_id_fk` FOREIGN KEY (`store_id`) REFERENCES `stores` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `store_contract`
--

LOCK TABLES `store_contract` WRITE;
/*!40000 ALTER TABLE `store_contract` DISABLE KEYS */;
/*!40000 ALTER TABLE `store_contract` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stores`
--

DROP TABLE IF EXISTS `stores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `stores` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `store_code` varchar(100) NOT NULL,
  `market_place_id` bigint unsigned NOT NULL,
  `is_marketplace` tinyint(1) NOT NULL DEFAULT '1',
  `user_id` bigint unsigned NOT NULL,
  `name` varchar(145) NOT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `whatsapp` varchar(15) DEFAULT NULL,
  `cnpj` varchar(30) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `iframe_google_maps` text,
  `opening_hours` text,
  `status` int DEFAULT '1',
  `domain` varchar(255) DEFAULT NULL,
  `subdomain` varchar(100) DEFAULT NULL,
  `facebook` varchar(255) DEFAULT NULL,
  `instagram` varchar(255) DEFAULT NULL,
  `youtube` varchar(255) DEFAULT NULL,
  `tiktok` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `stores_subdomain_uindex` (`subdomain`),
  KEY `stores_users_id_fk` (`user_id`),
  KEY `stores_tenants_id_fk` (`market_place_id`),
  CONSTRAINT `stores_tenants_id_fk` FOREIGN KEY (`market_place_id`) REFERENCES `market_places` (`id`),
  CONSTRAINT `stores_users_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stores`
--

LOCK TABLES `stores` WRITE;
/*!40000 ALTER TABLE `stores` DISABLE KEYS */;
INSERT INTO `stores` VALUES (32,'7fb0be18-cca6-4787-a516-fcecd23ba0fa',9,0,43,'PC Store','21 3176-8976','21 2019818181','09.564.766/0001-99','Rua de ipanema, 81 - Rio de Janeiro - RJ',NULL,'09:00 as 18:00',1,NULL,'pcstore.belltech.com','https://facebook.com/pcstore','https://instagram.com/perfilInsta',NULL,NULL,'2022-03-11 18:53:53','2022-03-11 18:53:53'),(35,'68897e71-9dbc-4df2-b4c8-b0f82cce0902',9,0,47,'Dog Store','21 3176-8976','21 2019818181','09.564.766/0001-99','Rua de ipanema, 81 - Rio de Janeiro - RJ',NULL,'09:00 as 18:00',1,NULL,'dogstore.belltech.com','https://facebook.com/dogstore','https://instagram.com/perfilInsta',NULL,NULL,'2022-03-12 02:51:49','2022-03-12 02:51:49'),(36,'6f1613b2-80f3-4e95-8dda-8e995e3f5a56',9,0,50,'Flamengo Store','21 3176-8976','21 2019818181','09.564.766/0001-99','Rua Gavea, 81 - Rio de Janeiro - RJ',NULL,'09:00 as 18:00',1,'flamengo.com.br',NULL,'https://facebook.com/flamengostore','https://instagram.com/perfilInsta',NULL,NULL,'2022-03-14 02:56:35','2022-03-14 02:56:35'),(39,'246e5a38-fa97-4ffa-a157-552cae1aa57d',9,0,55,'Nação Store','21 3176-8976','21 2019818181','09.564.766/0001-99','Rua Gavea, 81 - Rio de Janeiro - RJ',NULL,'09:00 as 18:00',1,'nacao.com.br',NULL,'https://facebook.com/nacaostore','https://instagram.com/perfilInsta',NULL,NULL,'2022-03-25 02:50:53','2022-03-25 02:50:53'),(40,'3316f10b-30a4-48ee-851f-f8fd3e1707cd',9,1,56,'Grow Store','21 3176-8976','21 2019818181','09.564.766/0001-99','Rua Gavea, 81 - Rio de Janeiro - RJ',NULL,'09:00 as 18:00',1,'grow.com.br',NULL,'https://facebook.com/growstore','https://instagram.com/perfilInsta',NULL,NULL,'2022-03-25 14:20:49','2022-03-25 14:20:49'),(47,'5119dd68-262f-46f8-a210-1bdee7375be8',9,1,63,'JJ Store','21 3176-8976','21 2019818181','09.564.766/0001-99','Rua Gavea, 81 - Rio de Janeiro - RJ',NULL,'09:00 as 18:00',1,'jj.com.br',NULL,'https://facebook.com/jjstore','https://instagram.com/perfilInsta',NULL,NULL,'2022-04-23 06:01:37','2022-04-23 06:01:37');
/*!40000 ALTER TABLE `stores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `technical_data`
--

DROP TABLE IF EXISTS `technical_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `technical_data` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `store_id` bigint unsigned NOT NULL,
  `name` varchar(100) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `technical_data_stores_id_fk` (`store_id`),
  CONSTRAINT `technical_data_stores_id_fk` FOREIGN KEY (`store_id`) REFERENCES `stores` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `technical_data`
--

LOCK TABLES `technical_data` WRITE;
/*!40000 ALTER TABLE `technical_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `technical_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `technical_data_item`
--

DROP TABLE IF EXISTS `technical_data_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `technical_data_item` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `technical_data_id` bigint unsigned NOT NULL,
  `name` varchar(100) NOT NULL,
  `value` varchar(255) NOT NULL,
  `description` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `technical_data_item_technical_data_id_fk` (`technical_data_id`),
  CONSTRAINT `technical_data_item_technical_data_id_fk` FOREIGN KEY (`technical_data_id`) REFERENCES `technical_data` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `technical_data_item`
--

LOCK TABLES `technical_data_item` WRITE;
/*!40000 ALTER TABLE `technical_data_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `technical_data_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `type` enum('client','seller','owner','ownerStore') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `market_place_id` bigint unsigned DEFAULT NULL,
  `profile_photo_path` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `users_tenants_id_fk` (`market_place_id`),
  CONSTRAINT `users_tenants_id_fk` FOREIGN KEY (`market_place_id`) REFERENCES `market_places` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (38,'owner',9,NULL,'Dayvison Silva','day@gmail.com',NULL,'$2y$10$qsvk8zeH5Mh8Yjrt9mf/M.t5pzM2/suNHhZn3mGEITpz75XXfWob.',NULL,'2022-03-11 06:20:48','2022-03-11 06:20:48'),(43,'ownerStore',9,NULL,'Guilherme Vieira','gvg@gmail.com',NULL,'$2y$10$qsvk8zeH5Mh8Yjrt9mf/M.t5pzM2/suNHhZn3mGEITpz75XXfWob.',NULL,'2022-03-11 21:53:53','2022-03-11 21:53:53'),(46,'seller',9,NULL,'Zico Antunes','zico@gmail.com',NULL,'$2y$10$jBcDjkF89if8Znhqehk61ORGl1b/dxyIOI.nObBoEKXG2SXnSOHOK',NULL,'2022-03-12 00:31:15','2022-03-12 00:31:15'),(47,'ownerStore',9,NULL,'Joao Paulo','jp@gmail.com',NULL,'$2y$10$Z143b8.tLXtKuFmprni2z.Dv5HoFSvJkmchPguptN/aOrBWQJq2r2',NULL,'2022-03-12 05:51:49','2022-03-12 05:51:49'),(50,'ownerStore',9,NULL,'Gustavo Flamengo','gustavo@gmail.com',NULL,'$2y$10$eAKndtryzr8Y2ZI..fmPdO9V23IHzC7dKSmi5zb4AV32WdrrJKAu2',NULL,'2022-03-14 05:56:34','2022-03-14 05:56:34'),(51,'client',9,NULL,'Gabi Gol','gabi@gmail.com',NULL,'$2y$10$4eXrBqG9RVRQOOb2wY2d0umrPLYSWIcfVaHUT0tA/NTCqUhBGq6PG',NULL,'2022-03-15 08:09:01','2022-03-15 08:09:01'),(55,'ownerStore',9,NULL,'Zico Flamengo','zicofla@gmail.com',NULL,'$2y$10$IXv3lRvVULvKH2MSQf7fc.A1aqW4rFom.zaoE4/b6Zchi9QVXUPsq',NULL,'2022-03-25 05:50:53','2022-03-25 05:50:53'),(56,'ownerStore',9,NULL,'Gabriel','gabigrow@gmail.com',NULL,'$2y$10$k91zHGN0CXyKnTY08qF9BuqGFrYmrvBncjXOW9qdc.pImqhsJUAi6',NULL,'2022-03-25 17:20:49','2022-03-25 17:20:49'),(63,'ownerStore',9,NULL,'Jorge Junior Vieira','jjgrow@gmail.com',NULL,'$2y$10$DjSQ6DSCt1bUwXYUXOwT5uYZl/.znMu1ykWnedSB4G9wYcli8H95a',NULL,'2022-04-23 09:01:37','2022-04-23 09:01:37');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_profile`
--

DROP TABLE IF EXISTS `users_profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users_profile` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `cpf` varchar(15) DEFAULT NULL,
  `cnpj` varchar(20) DEFAULT NULL,
  `rg` varchar(15) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `gender` char(20) DEFAULT NULL,
  `facebook` varchar(255) DEFAULT NULL,
  `instagram` varchar(255) DEFAULT NULL,
  `tiktok` varchar(255) DEFAULT NULL,
  `whatsapp` varchar(20) DEFAULT NULL,
  `mobile_phone` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `users_profile_users_id_fk` (`user_id`),
  CONSTRAINT `users_profile_users_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_profile`
--

LOCK TABLES `users_profile` WRITE;
/*!40000 ALTER TABLE `users_profile` DISABLE KEYS */;
INSERT INTO `users_profile` VALUES (1,38,'089.876.895-09',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2,43,'089.876.876-90',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(3,46,'089.876.895-09','09.265.126/0001-52','018332176-7',NULL,'M','https://facebook.com/storeface','https://instagram.com/perfilStore',NULL,'21 9 9637-9043','21 99786-8756'),(4,47,'089.876.876-90',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5,50,'089.876.876-90',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(6,51,'098.769.909-99',NULL,'119665423-9','2000-05-13','M','https://facebook.com/gabigol','https://instagram.com/perfil',NULL,'21 9 9675-9999','21 9 9675-9999');
/*!40000 ALTER TABLE `users_profile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wedding_list`
--

DROP TABLE IF EXISTS `wedding_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wedding_list` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `store_id` bigint unsigned NOT NULL,
  `customer_id` bigint unsigned NOT NULL,
  `wife` varchar(255) NOT NULL,
  `husband` varchar(255) DEFAULT NULL,
  `name` varchar(500) NOT NULL,
  `expired` datetime NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `wedding_list_stores_id_fk` (`store_id`),
  KEY `wedding_list_users_id_fk` (`customer_id`),
  CONSTRAINT `wedding_list_stores_id_fk` FOREIGN KEY (`store_id`) REFERENCES `stores` (`id`),
  CONSTRAINT `wedding_list_users_id_fk` FOREIGN KEY (`customer_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wedding_list`
--

LOCK TABLES `wedding_list` WRITE;
/*!40000 ALTER TABLE `wedding_list` DISABLE KEYS */;
INSERT INTO `wedding_list` VALUES (1,36,51,'Bruna Surfistinha',NULL,'Gabi Gol x Bruna Surfistinha','2022-10-03 00:00:00','2022-03-15 14:20:34','2022-03-15 14:20:34');
/*!40000 ALTER TABLE `wedding_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wedding_list_items`
--

DROP TABLE IF EXISTS `wedding_list_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wedding_list_items` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `product_id` bigint unsigned NOT NULL,
  `wedding_list_id` bigint unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `wedding_list_items_products_id_fk` (`product_id`),
  KEY `wedding_list_items_wedding_list_id_fk` (`wedding_list_id`),
  CONSTRAINT `wedding_list_items_products_id_fk` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
  CONSTRAINT `wedding_list_items_wedding_list_id_fk` FOREIGN KEY (`wedding_list_id`) REFERENCES `wedding_list` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wedding_list_items`
--

LOCK TABLES `wedding_list_items` WRITE;
/*!40000 ALTER TABLE `wedding_list_items` DISABLE KEYS */;
INSERT INTO `wedding_list_items` VALUES (1,19,1),(2,20,1);
/*!40000 ALTER TABLE `wedding_list_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wishlist`
--

DROP TABLE IF EXISTS `wishlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wishlist` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `store_id` bigint unsigned NOT NULL,
  `customer_id` bigint unsigned NOT NULL,
  `expired` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `wishlist_stores_id_fk` (`store_id`),
  KEY `wishlist_users_id_fk` (`customer_id`),
  CONSTRAINT `wishlist_stores_id_fk` FOREIGN KEY (`store_id`) REFERENCES `stores` (`id`),
  CONSTRAINT `wishlist_users_id_fk` FOREIGN KEY (`customer_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wishlist`
--

LOCK TABLES `wishlist` WRITE;
/*!40000 ALTER TABLE `wishlist` DISABLE KEYS */;
INSERT INTO `wishlist` VALUES (1,'minha lista',36,51,'2022-10-03 00:00:00','2022-03-15 15:09:14','2022-03-15 15:09:14'),(2,'minha lista',36,51,'2022-10-03 00:00:00','2022-03-15 15:29:18','2022-03-15 15:29:18');
/*!40000 ALTER TABLE `wishlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wishlist_items`
--

DROP TABLE IF EXISTS `wishlist_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wishlist_items` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `wishlist_id` bigint unsigned NOT NULL,
  `product_id` bigint unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `wishlist_items_wishlist_id_fk` (`wishlist_id`),
  KEY `wishlist_items_products_id_fk` (`product_id`),
  CONSTRAINT `wishlist_items_products_id_fk` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
  CONSTRAINT `wishlist_items_wishlist_id_fk` FOREIGN KEY (`wishlist_id`) REFERENCES `wishlist` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wishlist_items`
--

LOCK TABLES `wishlist_items` WRITE;
/*!40000 ALTER TABLE `wishlist_items` DISABLE KEYS */;
INSERT INTO `wishlist_items` VALUES (1,1,19),(2,1,20),(3,2,19),(4,2,20);
/*!40000 ALTER TABLE `wishlist_items` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-04-27  1:07:18
